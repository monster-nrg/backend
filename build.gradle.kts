val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val exposed_version: String by project
val jbcrypt_version: String by project

plugins {
  application
  kotlin("jvm") version "1.6.10"
}

group = "com.monsterenergy"
version = "0.0.1"

application {
  mainClass.set("com.monsterenergy.backend.ApplicationKt")
}

repositories {
  mavenCentral()
}

dependencies {
  // KTor
  implementation("io.ktor:ktor-server-core:$ktor_version")
  //// KTor plugins
  implementation("io.ktor:ktor-auth:$ktor_version")
  implementation("io.ktor:ktor-auth-jwt:$ktor_version")
  implementation("io.ktor:ktor-locations:$ktor_version")
  implementation("io.ktor:ktor-server-host-common:$ktor_version")
  implementation("io.ktor:ktor-gson:$ktor_version")
  implementation("io.ktor:ktor-server-netty:$ktor_version")

  // Logging
  implementation("ch.qos.logback:logback-classic:$logback_version")
  // Security
  implementation("org.mindrot:jbcrypt:$jbcrypt_version")
  // Serialization

  // Datasource
  //// ORM
  implementation("org.jetbrains.exposed:exposed-core:$exposed_version")
  implementation("org.jetbrains.exposed:exposed-dao:$exposed_version")
  implementation("org.jetbrains.exposed:exposed-jdbc:$exposed_version")
  implementation("org.jetbrains.exposed:exposed-java-time:$exposed_version")

  //// Connection pooling
  implementation("com.zaxxer:HikariCP:4.0.3")
  //// Driver
  runtimeOnly("org.postgresql:postgresql:42.3.1")

  // Test
  testImplementation("io.ktor:ktor-server-tests:$ktor_version")
  testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version")
  testImplementation("io.ktor:ktor-server-test-host:$ktor_version")
  testImplementation("org.jetbrains.kotlin:kotlin-test:$kotlin_version")
}
