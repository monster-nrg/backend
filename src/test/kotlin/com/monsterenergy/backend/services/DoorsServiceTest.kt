package com.monsterenergy.backend.services

import com.monsterenergy.backend.dto.door.DoorWithoutIdAndStatus
import com.monsterenergy.backend.repositories.tables.Doors
import com.monsterenergy.backend.services.impl.DoorsServiceImpl
import com.monsterenergy.backend.util.TestWithDb
import com.monsterenergy.backend.util.toExposedBlob
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class DoorsServiceTest : TestWithDb() {

  private var doorId: Long? = null

  @BeforeTest
  fun populateDb() {
    transaction {
      dropDb()
      doorId = DoorsServiceImpl.create(DoorWithoutIdAndStatus("test_door1_name", "desc", null)).id.value
      DoorsServiceImpl.create(
        DoorWithoutIdAndStatus(
          "test_door2_name",
          null,
          Base64.getEncoder().encodeToString("new_photo".toByteArray())
        )
      )
    }
  }

  @AfterTest
  fun dropDb() {
    transaction {
      Doors.deleteAll()
    }
  }

  @Test
  fun testGet() {
    val doors = DoorsServiceImpl.getAll()
    assertEquals(2, doors.size)
    assertEquals(2, doors.map {
      DoorsServiceImpl.getById(it.id.value)
    }.filter { it.name in listOf("test_door1_name", "test_door2_name", "test_Door_with_avatar") }.size)
  }

  @Test
  fun testUpdate() {

    val door = DoorsServiceImpl.update(doorId!!) {
      this.name = "new_name"
      this.description = null
      this.photo = Base64.getEncoder().encodeToString("new_photo".toByteArray()).toExposedBlob()
    }.let { DoorsServiceImpl.getById(it.id.value) }
    assertEquals("new_name", door.name)
    assertEquals(null, door.description)
    assertEquals("new_photo", String(door.photo!!.bytes))
  }

  @Test
  fun testDelete() {
    DoorsServiceImpl.deleteById(doorId!!)

    val doors = DoorsServiceImpl.getAll()
    assertEquals(1, doors.size)
    assertEquals(1, doors.map {
      DoorsServiceImpl.getById(it.id.value)
    }.filter { it.name in listOf("test_door2_name") }.size)
  }
}
