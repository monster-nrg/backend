package com.monsterenergy.backend.services

import com.monsterenergy.backend.dto.user.UserWithoutId
import com.monsterenergy.backend.repositories.tables.Users
import com.monsterenergy.backend.services.impl.UsersServiceImpl
import com.monsterenergy.backend.util.TestWithDb
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.statements.api.ExposedBlob
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDate
import java.util.*
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class UsersServiceTest : TestWithDb() {

  private var userId: Long? = null
  private var userWithAvatarId: Long? = null

  @BeforeTest
  fun populateDb() {
    transaction {
      dropDb()
      userId = UsersServiceImpl.create(
        UserWithoutId(
          "test_user1_name",
          "test_user1_sname",
          "test_user1_lname",
          LocalDate.now(),
          null
        )
      ).id.value

      UsersServiceImpl.create(
        UserWithoutId(
          "test_user2_name",
          "test_user2_sname",
          "test_user2_lname",
          LocalDate.now(),
          null
        )
      )

      userWithAvatarId = UsersServiceImpl.create(
        UserWithoutId(
          "test_user_with_avatar",
          "test_user_with_avatars",
          "test_user_with_avatarl",
          LocalDate.now(),
          Base64.getEncoder().encodeToString("avatar".toByteArray())
        )
      ).id.value
    }
  }

  @AfterTest
  fun dropDb() {
    transaction {
      Users.deleteAll()
    }
  }

  @Test
  fun testGet() {
    val users = UsersServiceImpl.getAll()
    assertEquals(3, users.size)
    assertEquals(3, users.map {
      UsersServiceImpl.getById(it.id.value)
    }.filter { it.firstname in listOf("test_user1_name", "test_user2_name", "test_user_with_avatar") }.size)
  }

  @Test
  fun testUpdate() {

    val user = UsersServiceImpl.update(userId!!) {
      this.firstname = "new_name"
    }.let { UsersServiceImpl.getById(it.id.value) }
    assertEquals("new_name", user.firstname)
  }

  @Test
  fun updateAvatar() {

    UsersServiceImpl.update(userId!!) {
      this.avatar = ExposedBlob("new avatar".toByteArray())
    }.let {
      assertEquals("new avatar", String(UsersServiceImpl.getById(it.id.value).avatar!!.bytes))
    }


    UsersServiceImpl.update(userWithAvatarId!!) {
      this.avatar = null
    }.let {
      assertEquals(null, UsersServiceImpl.getById(it.id.value).avatar)
    }
  }

  @Test
  fun testDelete() {
    UsersServiceImpl.deleteById(userId!!)

    val users = UsersServiceImpl.getAll()
    assertEquals(2, users.size)
    assertEquals(2, users.map {
      UsersServiceImpl.getById(it.id.value)
    }.filter { it.firstname in listOf("test_user2_name", "test_user_with_avatar") }.size)
  }
}
