package com.monsterenergy.backend.services

import com.monsterenergy.backend.dto.permission.PermissionWithoutId
import com.monsterenergy.backend.repositories.tables.Permissions
import com.monsterenergy.backend.services.impl.PermissionsServiceImpl
import com.monsterenergy.backend.util.TestWithDb
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class PermissionsServiceTest : TestWithDb() {

  private var permissionId: Long? = null

  @BeforeTest
  fun populateDb() {
    transaction {
      dropDb()
      permissionId =
        PermissionsServiceImpl.create(PermissionWithoutId("test_permission1_cname", "test_permission1_name")).id.value
      PermissionsServiceImpl.create(PermissionWithoutId("test_permission2_cname", "test_permission2_name"))
    }
  }

  @AfterTest
  fun dropDb() {
    transaction {
      Permissions.deleteAll()
    }
  }

  @Test
  fun testGet() {
    val permissions = PermissionsServiceImpl.getAll()
    assertEquals(2, permissions.size)
    assertEquals(2, permissions.map {
      PermissionsServiceImpl.getById(it.id.value)
    }.filter { it.name in listOf("test_permission1_name", "test_permission2_name") }.size)
  }

  @Test
  fun testUpdate() {

    val permission = PermissionsServiceImpl.update(permissionId!!) {
      this.name = "new_name"
    }.let { PermissionsServiceImpl.getById(it.id.value) }
    assertEquals("new_name", permission.name)
    assertEquals("test_permission1_cname", permission.codename)
  }

  @Test
  fun testDelete() {
    PermissionsServiceImpl.deleteById(permissionId!!)

    val permissions = PermissionsServiceImpl.getAll()
    assertEquals(1, permissions.size)
    assertEquals(1, permissions.map {
      PermissionsServiceImpl.getById(it.id.value)
    }.filter { it.name in listOf("test_permission2_name") }.size)
  }
}
