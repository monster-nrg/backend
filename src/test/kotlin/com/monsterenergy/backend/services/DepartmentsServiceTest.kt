package com.monsterenergy.backend.services

import com.monsterenergy.backend.dto.department.DepartmentWithoutId
import com.monsterenergy.backend.repositories.tables.Departments
import com.monsterenergy.backend.services.impl.DepartmentsServiceImpl
import com.monsterenergy.backend.util.TestWithDb
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class DepartmentsServiceTest : TestWithDb() {

  private var departmentId: Long? = null

  @BeforeTest
  fun populateDb() {
    transaction {
      dropDb()
      departmentId = DepartmentsServiceImpl.create(DepartmentWithoutId("test_Department1_name")).id.value
      DepartmentsServiceImpl.create(DepartmentWithoutId("test_Department2_name"))
    }
  }

  @AfterTest
  fun dropDb() {
    transaction {
      Departments.deleteAll()
    }
  }

  @Test
  fun testGet() {
    val departments = DepartmentsServiceImpl.getAll()
    assertEquals(2, departments.size)
    assertEquals(2, departments.map {
      DepartmentsServiceImpl.getById(it.id.value)
    }.filter {
      it.name in listOf(
        "test_Department1_name",
        "test_Department2_name",
        "test_Department_with_avatar"
      )
    }.size)
  }

  @Test
  fun testUpdate() {

    val department = DepartmentsServiceImpl.update(departmentId!!) {
      this.name = "new_name"
    }.let { DepartmentsServiceImpl.getById(it.id.value) }
    assertEquals("new_name", department.name)
  }

  @Test
  fun testDelete() {
    DepartmentsServiceImpl.deleteById(departmentId!!)

    val departments = DepartmentsServiceImpl.getAll()
    assertEquals(1, departments.size)
    assertEquals(1, departments.map {
      DepartmentsServiceImpl.getById(it.id.value)
    }.filter { it.name in listOf("test_Department2_name") }.size)
  }
}
