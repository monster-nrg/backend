package com.monsterenergy.backend.services

import com.monsterenergy.backend.dto.child.ChildWithoutId
import com.monsterenergy.backend.repositories.tables.Children
import com.monsterenergy.backend.services.impl.ChildrenServiceImpl
import com.monsterenergy.backend.util.TestWithDb
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.statements.api.ExposedBlob
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDate
import java.util.*
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class ChildrenServiceTest : TestWithDb() {

  private var childId: Long? = null
  private var childWithAvatarId: Long? = null

  @BeforeTest
  fun populateDb() {
    transaction {
      dropDb()
      childId = ChildrenServiceImpl.create(
        ChildWithoutId(
          "test_child1_name",
          "test_child1_sname",
          "test_child1_lname",
          LocalDate.now()
        )
      ).id.value

      ChildrenServiceImpl.create(
        ChildWithoutId(
          "test_child2_name",
          "test_child2_sname",
          "test_child2_lname",
          LocalDate.now()
        )
      )

      childWithAvatarId = ChildrenServiceImpl.create(
        ChildWithoutId(
          "test_child_with_avatar",
          "test_child_with_avatars",
          "test_child_with_avatarl",
          LocalDate.now(),
          Base64.getEncoder().encodeToString("avatar".toByteArray())
        )
      ).id.value
    }
  }

  @AfterTest
  fun dropDb() {
    transaction {
      Children.deleteAll()
    }
  }

  @Test
  fun testGet() {
    val children = ChildrenServiceImpl.getAll()
    assertEquals(3, children.size)
    assertEquals(3, children.map {
      ChildrenServiceImpl.getById(it.id.value)
    }.filter { it.firstname in listOf("test_child1_name", "test_child2_name", "test_child_with_avatar") }.size)
  }

  @Test
  fun testUpdate() {

    val child = ChildrenServiceImpl.update(childId!!) {
      this.firstname = "new_name"
    }.let { ChildrenServiceImpl.getById(it.id.value) }
    assertEquals("new_name", child.firstname)
  }

  @Test
  fun updateAvatar() {

    ChildrenServiceImpl.update(childId!!) {
      this.avatar = ExposedBlob("new avatar".toByteArray())
    }.let {
      assertEquals("new avatar", String(ChildrenServiceImpl.getById(it.id.value).avatar!!.bytes))
    }


    ChildrenServiceImpl.update(childWithAvatarId!!) {
      this.avatar = null
    }.let {
      assertEquals(null, ChildrenServiceImpl.getById(it.id.value).avatar)
    }
  }

  @Test
  fun testDelete() {
    ChildrenServiceImpl.deleteById(childId!!)

    val children = ChildrenServiceImpl.getAll()
    assertEquals(2, children.size)
    assertEquals(2, children.map {
      ChildrenServiceImpl.getById(it.id.value)
    }.filter { it.firstname in listOf("test_child2_name", "test_child_with_avatar") }.size)
  }
}
