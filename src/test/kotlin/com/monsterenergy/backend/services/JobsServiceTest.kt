package com.monsterenergy.backend.services

import com.monsterenergy.backend.dto.department.DepartmentWithoutId
import com.monsterenergy.backend.dto.job.JobWithoutId
import com.monsterenergy.backend.repositories.tables.Departments
import com.monsterenergy.backend.repositories.tables.Jobs
import com.monsterenergy.backend.services.impl.DepartmentsServiceImpl
import com.monsterenergy.backend.services.impl.JobsServiceImpl
import com.monsterenergy.backend.util.TestWithDb
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class JobsServiceTest : TestWithDb() {

  private var jobId: Long? = null

  @BeforeTest
  fun populateDb() {
    transaction {
      dropDb()
      jobId = JobsServiceImpl.create(
        JobWithoutId(
          DepartmentsServiceImpl.create(DepartmentWithoutId("test_Department1_name")).id.value,
          "test_job1_name"
        )
      ).id.value
      JobsServiceImpl.create(
        JobWithoutId(
          DepartmentsServiceImpl.create(DepartmentWithoutId("test_Department2_name")).id.value,
          "test_job2_name"
        )
      )
    }
  }

  @AfterTest
  fun dropDb() {
    transaction {
      Jobs.deleteAll()
      Departments.deleteAll()
    }
  }

  @Test
  fun testGet() {
    val jobs = JobsServiceImpl.getAll()
    assertEquals(2, jobs.size)
    assertEquals(2, jobs.map {
      JobsServiceImpl.getById(it.id.value)
    }.filter { it.name in listOf("test_job1_name", "test_job2_name") }.size)
  }

  @Test
  fun testUpdate() {

    val job = JobsServiceImpl.update(jobId!!) {
      this.name = "new_name"
    }.let { JobsServiceImpl.getById(it.id.value) }
    assertEquals("new_name", job.name)
  }

  @Test
  fun testDelete() {
    JobsServiceImpl.deleteById(jobId!!)

    val jobs = JobsServiceImpl.getAll()
    assertEquals(1, jobs.size)
    assertEquals(1, jobs.map {
      JobsServiceImpl.getById(it.id.value)
    }.filter { it.name in listOf("test_job2_name") }.size)
  }
}
