package com.monsterenergy.backend.services

import com.auth0.jwt.JWT
import com.monsterenergy.backend.dto.account.UsernamePassword
import com.monsterenergy.backend.repositories.dao.Account
import com.monsterenergy.backend.repositories.dao.User
import com.monsterenergy.backend.repositories.tables.Accounts
import com.monsterenergy.backend.repositories.tables.Users
import com.monsterenergy.backend.services.impl.AccountsServiceImpl
import com.monsterenergy.backend.util.TestWithDb
import com.monsterenergy.backend.util.assertException
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.mindrot.jbcrypt.BCrypt
import java.time.LocalDate
import java.util.*
import kotlin.test.*


class AccountsServiceTest : TestWithDb() {

  @BeforeTest
  fun populateDb() {
    transaction {
      dropDb()
      Account.new {
        this.username = "test_user"
        this.password = BCrypt.hashpw("test_pass", BCrypt.gensalt())
        this.user = User.new {
          this.firstname = "user"
          this.lastname = "userovich"
          this.birthDate = LocalDate.now()
        }
      }

      Account.new {
        this.username = "other_user"
        this.password = BCrypt.hashpw("test_pass", BCrypt.gensalt())
        this.user = User.new {
          this.firstname = "user"
          this.lastname = "userovich"
          this.birthDate = LocalDate.now()
        }
      }
    }
  }

  @AfterTest
  fun dropDb() {
    transaction {
      Accounts.deleteAll()
      Users.deleteAll()
    }
  }

  @Test
  fun testLoginLogout() {
    val (user, token) = AccountsServiceImpl.login(UsernamePassword("test_user", "test_pass"))
    assertEquals(user.username, "test_user")
    assertTrue(token.token.isNotEmpty())
    AccountsServiceImpl.logout(UUID.fromString(JWT.decode(token.token).id))
  }

  @Test
  fun testLoginError() {
    assertException<AuthenticationException> {
      AccountsServiceImpl.login(UsernamePassword("test_user", "not_test_pass"))
    }
    assertException<Throwable> {
      AccountsServiceImpl.login(UsernamePassword("not_test_user", "test_pass"))
    }
  }

  @Test
  fun testGetByUsername() {
    val user = AccountsServiceImpl.getByUsername("test_user")
    assertEquals(user.username, "test_user")
    assertException<Throwable> {
      AccountsServiceImpl.getByUsername("not_test_user")
    }
  }

  @Test
  fun testGetById() {
    var user = AccountsServiceImpl.getByUsername("test_user")
    assertEquals(user.username, "test_user")
    user = AccountsServiceImpl.getById(user.id.value)
    assertEquals(user.username, "test_user")
    assertException<Throwable> {
      AccountsServiceImpl.getById(0)
    }
  }

  @Test
  fun testGetByAll() {
    val users = AccountsServiceImpl.getAll()
    assert(users.size == 2)
    with(users.map { it.username }) {
      assert(this.contains("test_user"))
      assert(this.contains("other_user"))
    }
  }

  @Test
  fun testUpdate() {
    val user = AccountsServiceImpl.getByUsername("other_user")
    AccountsServiceImpl.update(user.id.value) {
      this.password = "new_pass"
    }
    assertEquals(AccountsServiceImpl.getByUsername("other_user").password, "new_pass")
  }
}
