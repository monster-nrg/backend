package com.monsterenergy.backend.services

import com.monsterenergy.backend.dto.door.DoorWithoutIdAndStatus
import com.monsterenergy.backend.dto.fear.CreateFearRequestDto
import com.monsterenergy.backend.dto.user.UserWithoutId
import com.monsterenergy.backend.repositories.dao.Fear
import com.monsterenergy.backend.repositories.tables.Doors
import com.monsterenergy.backend.repositories.tables.FearStatus
import com.monsterenergy.backend.repositories.tables.Fears
import com.monsterenergy.backend.repositories.tables.Users
import com.monsterenergy.backend.services.impl.DoorsServiceImpl
import com.monsterenergy.backend.services.impl.FearsServiceImpl
import com.monsterenergy.backend.services.impl.UsersServiceImpl
import com.monsterenergy.backend.util.TestWithDb
import com.monsterenergy.backend.util.assertException
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDate
import java.time.LocalDateTime
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class FearsServiceTest : TestWithDb() {

  private var fearIds = mutableListOf<Long>()

  fun createFear(i: Int): Fear {
    val user = UsersServiceImpl.create(UserWithoutId("name$i", "sname$i", "lname$i", LocalDate.now(), null))
    val door = DoorsServiceImpl.create(DoorWithoutIdAndStatus("door$i", "dec$i", null))
    return FearsServiceImpl.create(
      CreateFearRequestDto(
        user.id.value,
        door.id.value,
        LocalDateTime.now(),
        5.5f,
        "comm$i"
      )
    )
  }

  @BeforeTest
  fun populateDb() {
    transaction {
      dropDb()
      fearIds.add(createFear(1).id.value)
      fearIds.add(createFear(2).id.value)
    }
  }

  @AfterTest
  fun dropDb() {
    transaction {
      Fears.deleteAll()
      Users.deleteAll()
      Doors.deleteAll()
    }
  }

  @Test
  fun testGet() {
    val fears = FearsServiceImpl.getAll()
    assertEquals(fearIds.size, fears.size)
    assertEquals(fearIds.size, fears.map {
      FearsServiceImpl.getById(it.id.value)
    }.filter { it.id.value in fearIds }.size)
  }

  @Test
  fun testStartFear() {

    FearsServiceImpl.startFear(fearIds[0])
    val fear = FearsServiceImpl.getById(fearIds[0])
    assertEquals(FearStatus.InProgress, fear.status)
    assert(fear.startDatetime != null)

    assertException<IllegalStateException> {
      FearsServiceImpl.startFear(fearIds[0])
    }

    assertException<IllegalStateException> {
      FearsServiceImpl.cancelFear(fearIds[0])
    }
  }

  @Test
  fun testFinishFear() {

    FearsServiceImpl.startFear(fearIds[0])
    FearsServiceImpl.finishFear(fearIds[0])
    val fear = FearsServiceImpl.getById(fearIds[0])
    assertEquals(FearStatus.Finished, fear.status)
    assert(fear.finishDatetime != null)

    assertException<IllegalStateException> {
      FearsServiceImpl.finishFear(fearIds[0])
    }
    assertException<IllegalStateException> {
      FearsServiceImpl.cancelFear(fearIds[0])
    }

    assertException<IllegalStateException> {
      FearsServiceImpl.finishFear(fearIds[1])
    }
  }

  @Test
  fun testCancelFear() {

    FearsServiceImpl.cancelFear(fearIds[1])
    val fear = FearsServiceImpl.getById(fearIds[1])
    assertEquals(FearStatus.Cancelled, fear.status)

    assertException<IllegalStateException> {
      FearsServiceImpl.startFear(fearIds[1])
    }
    assertException<IllegalStateException> {
      FearsServiceImpl.finishFear(fearIds[1])
    }

    assertException<IllegalStateException> {
      FearsServiceImpl.cancelFear(fearIds[1])
    }
  }
}
