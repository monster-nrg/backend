package com.monsterenergy.backend.util

import com.monsterenergy.backend.repositories.config.DataSource

open class TestWithDb {

  companion object {
    val db by lazy { DataSource.configureDatasource() }
  }

  init {
    db
  }
}
