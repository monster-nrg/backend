package com.monsterenergy.backend.util

import kotlin.reflect.full.isSubclassOf

inline fun <reified T : Throwable> assertException(f: () -> Unit) {
  try {
    f()
  } catch (e: Throwable) {
    assert(e::class.isSubclassOf(T::class))
    return
  }
  assert(false)


}
