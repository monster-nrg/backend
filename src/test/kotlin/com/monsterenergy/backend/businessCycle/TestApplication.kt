package com.monsterenergy.backend.businessCycle

import com.monsterenergy.backend.plugins.*
import io.ktor.application.*

fun Application.testApplication() {
  configureHTTP()
  configureMonitoring()
  configureSecurity()
  configureRouting()
  configureSerialization()
}
