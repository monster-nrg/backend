package com.monsterenergy.backend.businessCycle.store

import com.google.gson.Gson
import com.monsterenergy.backend.businessCycle.testApplication
import com.monsterenergy.backend.repositories.dao.Account
import com.monsterenergy.backend.repositories.dao.Door
import com.monsterenergy.backend.repositories.dao.User
import com.monsterenergy.backend.util.SuccessResponse
import com.monsterenergy.backend.util.TestWithDb
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.server.testing.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mindrot.jbcrypt.BCrypt
import java.time.LocalDate
import kotlin.test.assertEquals

class UtilizeDoorTest : TestWithDb() {
  private var _user: User? = null
  private var _account: Account? = null
  private var _door: Door? = null

  @Before
  fun setUp() {
    transaction {
      _user = User.new {
        firstname = "a"
        surname = "b"
        lastname = "c"
        birthDate = LocalDate.now()
      }
      _account = Account.new {
        this.user = _user as User
        this.username = "test"
        this.password = BCrypt.hashpw("test", BCrypt.gensalt())
      }
      _door = Door.new {
        this.name = "aaa"
        this.description = "bbb"
      }
    }
  }

  @After
  fun tearDown() {
    transaction {
      _account?.delete()
      _user?.delete()
    }
  }

  @Test
  fun viewProfile() {
    var token: String? = null
    withTestApplication(Application::testApplication) {
      with(handleRequest(HttpMethod.Post, "/api/accounts/login") {
        addHeader("Content-Type", "application/json")
        setBody("""{ "username":"test", "password": "test" }""")
      }) {
        assertEquals(HttpStatusCode.OK, response.status())
        assert(response.content?.contains("token") == true)
        token = (Gson().runCatching {
          fromJson(response.content, SuccessResponse::class.java)
            .payload as String
        }).onFailure { return@withTestApplication }.getOrNull()
      }
      with(handleRequest(HttpMethod.Post, "/api/doors/${_door?.id?.value}") {
        addHeader("Content-Type", "application/json")
        addHeader("Authorization", "Bearer $token")
        setBody("""{"name":"The door"}""")
      }) {
        assertEquals(HttpStatusCode.NoContent, response.status())
      }
    }
  }
}
