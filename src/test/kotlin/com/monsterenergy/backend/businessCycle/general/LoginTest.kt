package com.monsterenergy.backend.businessCycle.general

import com.monsterenergy.backend.businessCycle.testApplication
import com.monsterenergy.backend.repositories.dao.Account
import com.monsterenergy.backend.repositories.dao.User
import com.monsterenergy.backend.util.TestWithDb
import com.monsterenergy.backend.util.assertException
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.server.testing.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mindrot.jbcrypt.BCrypt
import java.time.LocalDate
import kotlin.test.assertEquals

class LoginTest : TestWithDb() {
  private var _user: User? = null
  private var _account: Account? = null

  @Before
  fun setUp() {
    transaction {
      _user = User.new {
        firstname = "a"
        surname = "b"
        lastname = "c"
        birthDate = LocalDate.now()
      }
      _account = Account.new {
        this.user = _user as User
        this.username = "test"
        this.password = BCrypt.hashpw("test", BCrypt.gensalt())
      }
    }
  }

  @After
  fun tearDown() {
    transaction {
      _account?.delete()
      _user?.delete()
    }
  }

  @Test
  fun loginFailed() {
    assertException<NotImplementedError> {
      withTestApplication(Application::testApplication) {
        with(handleRequest(HttpMethod.Post, "/api/accounts/login") {
          addHeader("Content-Type", "application/json")
          setBody("""{ "username":"test_failed", "password": "test" }""")
        }) {
          assertEquals(HttpStatusCode.OK, response.status())
          assert(response.content?.contains("token") == true)
        }
      }
    }
  }

  @Test
  fun loginSuccessful() {
    withTestApplication(Application::testApplication) {
      with(handleRequest(HttpMethod.Post, "/api/accounts/login") {
        addHeader("Content-Type", "application/json")
        setBody("""{ "username":"test", "password": "test" }""")
      }) {
        assertEquals(HttpStatusCode.OK, response.status())
        assert(response.content?.contains("token") == true)
      }
    }
  }
}

