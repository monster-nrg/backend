package com.monsterenergy.backend.repositories.tables

import org.jetbrains.exposed.sql.Table

object JobPermissions : Table() {
  val jobId = reference("job_id", Jobs.id)
  val permissionId = reference("permission_id", Permissions)

  override val primaryKey = PrimaryKey(jobId, permissionId)
}
