package com.monsterenergy.backend.repositories.tables

import org.jetbrains.exposed.sql.Table

object UserJobs : Table() {
  val userId = reference("user_id", Users)
  val jobId = reference("job_id", Jobs)

  override val primaryKey = PrimaryKey(userId, jobId)
}
