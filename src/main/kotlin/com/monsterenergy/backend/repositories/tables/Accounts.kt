package com.monsterenergy.backend.repositories.tables

import org.jetbrains.exposed.dao.id.IdTable

object Accounts : IdTable<Long>("account") {
  override val id = long("id").entityId().references(Users.id)
  override val primaryKey = PrimaryKey(id)

  val username = varchar("username", 255).uniqueIndex()
  val password = varchar("password", 255)
  val tokenId = uuid("token_id").uniqueIndex().nullable()
}
