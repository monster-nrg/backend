package com.monsterenergy.backend.repositories.tables

enum class DoorStatus {
  Stored,
  Moving,
  Busy
}
