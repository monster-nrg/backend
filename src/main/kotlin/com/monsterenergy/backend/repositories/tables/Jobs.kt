package com.monsterenergy.backend.repositories.tables

import org.jetbrains.exposed.dao.id.LongIdTable

object Jobs : LongIdTable() {
  val departmentId = reference("department_id", Departments)
  val name = varchar("name", 255)

  init {
    uniqueIndex(departmentId, name)
  }
}
