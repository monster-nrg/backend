package com.monsterenergy.backend.repositories.tables

import org.jetbrains.exposed.dao.id.LongIdTable

object Departments : LongIdTable("departments") {
  val name = varchar("name", 255).uniqueIndex()
}

