package com.monsterenergy.backend.repositories.tables

import org.jetbrains.exposed.dao.id.LongIdTable

object Permissions : LongIdTable() {
  val codename = varchar("codename", 32).uniqueIndex()
  val name = varchar("name", 255).nullable()
}
