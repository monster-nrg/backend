package com.monsterenergy.backend.repositories.tables

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.javatime.datetime

object Fears : LongIdTable() {
  val executorId = reference("executor_id", Users)
  val doorId = reference("door_id", Doors)
  val status = enumeration("status", FearStatus::class)
  val expectedDatetime = datetime("expected_datetime")
  val startDatetime = datetime("start_datetime").nullable()
  val finishDatetime = datetime("finish_datetime").nullable()
  val expectedPower = float("expected_power").nullable()
  val actualPower = float("actual_power").nullable()
  val comment = varchar("comment", 255).nullable()
}
