package com.monsterenergy.backend.repositories.tables

import org.jetbrains.exposed.sql.Table

object ChildDoors : Table() {
  val childId = reference("child_id", Children)
  val doorId = reference("door_id", Doors)

  override val primaryKey = PrimaryKey(childId, doorId)
}
