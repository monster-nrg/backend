package com.monsterenergy.backend.repositories.tables

import org.jetbrains.exposed.dao.id.LongIdTable

object Doors : LongIdTable() {
  val status = enumeration("status", DoorStatus::class)
  val name = varchar("name", 255)
  val description = varchar("description", 4095).nullable()
  val photo = blob("photo").nullable()
}
