package com.monsterenergy.backend.repositories.tables

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.javatime.date

object Users : LongIdTable("users") {
  val firstname = varchar("firstname", 255)
  val surname = varchar("surname", 255).nullable()
  val lastname = varchar("lastname", 255)
  val birthDate = date("birthDate")
  val avatar = blob("avatar").nullable()
}
