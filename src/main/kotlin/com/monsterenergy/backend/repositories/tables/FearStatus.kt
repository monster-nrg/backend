package com.monsterenergy.backend.repositories.tables

enum class FearStatus {
  Planned,
  InProgress,
  Finished,
  Cancelled
}
