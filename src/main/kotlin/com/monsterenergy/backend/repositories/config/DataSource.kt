package com.monsterenergy.backend.repositories.config

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.DatabaseConfig

object DataSource {
  fun configureDatasource(): Database {
    val config = HikariConfig("/hikari.properties")
    val dataSource = HikariDataSource(config)
    val databaseConfig = DatabaseConfig {
      keepLoadedReferencesOutOfTransaction = true
    }
    return Database.connect(dataSource, databaseConfig = databaseConfig)
  }
}
