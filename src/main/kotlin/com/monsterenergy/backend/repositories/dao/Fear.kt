package com.monsterenergy.backend.repositories.dao

import com.monsterenergy.backend.repositories.tables.Fears
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class Fear(id: EntityID<Long>): LongEntity(id) {
  companion object : LongEntityClass<Fear>(Fears)

  var executor by User referencedOn Fears.executorId
  var executorId by Fears.executorId

  var door by Door referencedOn Fears.doorId
  var doorId by Fears.doorId

  var expectedDatetime by Fears.expectedDatetime
  var startDatetime by Fears.startDatetime
  var finishDatetime by Fears.finishDatetime
  var expectedPower by Fears.expectedPower
  var actualPower by Fears.actualPower
  var comment by Fears.comment
  var status by Fears.status
}
