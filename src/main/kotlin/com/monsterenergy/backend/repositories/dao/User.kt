package com.monsterenergy.backend.repositories.dao

import com.monsterenergy.backend.repositories.tables.UserJobs
import com.monsterenergy.backend.repositories.tables.Users
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class User(id: EntityID<Long>) : LongEntity(id) {
  companion object : LongEntityClass<User>(Users)

  var firstname by Users.firstname
  var surname by Users.surname
  var lastname by Users.lastname
  var birthDate by Users.birthDate
  var avatar by Users.avatar
  var jobs by Job via UserJobs
}
