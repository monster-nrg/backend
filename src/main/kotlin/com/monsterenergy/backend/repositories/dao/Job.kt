package com.monsterenergy.backend.repositories.dao

import com.monsterenergy.backend.repositories.tables.JobPermissions
import com.monsterenergy.backend.repositories.tables.Jobs
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class Job(id: EntityID<Long>) : LongEntity(id) {
  companion object : LongEntityClass<Job>(Jobs)

  var department by Department referencedOn Jobs.departmentId
  val departmentId by Jobs.departmentId

  var name by Jobs.name
  var permissions by Permission via JobPermissions
}
