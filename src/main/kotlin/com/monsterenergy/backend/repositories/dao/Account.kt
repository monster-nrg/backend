package com.monsterenergy.backend.repositories.dao

import com.monsterenergy.backend.repositories.tables.Accounts
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import java.util.*

class Account(id: EntityID<Long>) : LongEntity(id) {
  companion object : LongEntityClass<Account>(Accounts) {
    fun findByUsername(username: String): Account? =
      find { Accounts.username eq username }.singleOrNull()

    fun findByTokenId(tokenId: UUID): Account? =
      find { Accounts.tokenId eq tokenId }.singleOrNull()
  }

  var user by User referencedOn Accounts.id

  var username by Accounts.username
  var password by Accounts.password
  var tokenId by Accounts.tokenId
}

