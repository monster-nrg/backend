package com.monsterenergy.backend.repositories.dao

import com.monsterenergy.backend.repositories.tables.ChildDoors
import com.monsterenergy.backend.repositories.tables.Children
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class Child(id: EntityID<Long>) : LongEntity(id) {
  companion object : LongEntityClass<Child>(Children)

  var firstname by Children.firstname
  var surname by Children.surname
  var lastname by Children.lastname
  var birthDate by Children.birthDate
  var avatar by Children.avatar

  var doors by Door via ChildDoors
}
