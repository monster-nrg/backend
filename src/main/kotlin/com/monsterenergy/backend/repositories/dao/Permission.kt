package com.monsterenergy.backend.repositories.dao

import com.monsterenergy.backend.repositories.tables.Permissions
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class Permission(id: EntityID<Long>) : LongEntity(id)  {
  companion object : LongEntityClass<Permission>(Permissions) {
    fun findByCodename(codename: String): Permission? =
      find { Permissions.codename eq codename }.singleOrNull()
  }

  var codename by Permissions.codename
  var name by Permissions.name
}
