package com.monsterenergy.backend.repositories.dao

import com.monsterenergy.backend.repositories.tables.Departments
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class Department(id: EntityID<Long>) : LongEntity(id) {
  companion object : LongEntityClass<Department>(Departments)

  var name by Departments.name
}
