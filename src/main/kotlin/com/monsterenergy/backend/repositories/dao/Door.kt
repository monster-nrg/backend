package com.monsterenergy.backend.repositories.dao

import com.monsterenergy.backend.repositories.tables.ChildDoors
import com.monsterenergy.backend.repositories.tables.Doors
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class Door(id: EntityID<Long>) : LongEntity(id) {
  companion object : LongEntityClass<Door>(Doors)

  var status by Doors.status
  var name by Doors.name
  var description by Doors.description
  var photo by Doors.photo

  var children by Child via ChildDoors
}
