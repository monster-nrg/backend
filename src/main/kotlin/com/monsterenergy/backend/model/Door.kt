package com.monsterenergy.backend.model

import java.util.Optional as OptionalValue

// @formatter:off
object Door {
  interface Id { val id: Long }
  interface Status {
    val status: DoorStatus
    interface Optional { val status: OptionalValue<DoorStatus>? }
  }
  interface Name {
    val name: String
    interface Optional { val name: OptionalValue<String>? }
  }
  interface Description {
    val description: String?
    interface Optional { val description: OptionalValue<String>? }
  }
  interface Photo {
    val photo: String?
    interface Optional { val photo: OptionalValue<String>? }
  }
}
// @formatter:on
