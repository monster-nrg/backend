package com.monsterenergy.backend.model

import java.util.*
import java.util.Optional as OptionalValue

// @formatter:off
object Account {
  interface Id { val id: Long }
  interface Username {
    val username: String
    interface Optional { val username: OptionalValue<String>? }
  }
  interface Password {
    val password: String
    interface Optional { val password: OptionalValue<String>? }
  }
  interface TokenId { val tokenId: UUID }
}
// @formatter:on
