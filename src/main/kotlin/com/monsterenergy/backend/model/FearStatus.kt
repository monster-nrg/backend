package com.monsterenergy.backend.model

import com.monsterenergy.backend.repositories.tables.FearStatus as TableFearStatus

enum class FearStatus(val status: String) {
  Planned("planned"),
  InProgress("in progress"),
  Finished("finished"),
  Cancelled("cancelled");

  fun toEntity() =
    when (this) {
      Planned -> TableFearStatus.Planned
      InProgress -> TableFearStatus.InProgress
      Finished -> TableFearStatus.Finished
      Cancelled -> TableFearStatus.Cancelled
    }

  companion object {
    fun of(entity: TableFearStatus) =
      when (entity) {
        TableFearStatus.Planned -> Planned
        TableFearStatus.InProgress -> InProgress
        TableFearStatus.Finished -> Finished
        TableFearStatus.Cancelled -> Cancelled
      }
  }
}
