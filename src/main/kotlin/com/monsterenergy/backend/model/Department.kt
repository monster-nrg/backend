package com.monsterenergy.backend.model

import java.util.Optional as OptionalValue

// @formatter:off
object Department {
  interface Id { val id: Long }
  interface Name {
    val name: String
    interface Optional { val name: OptionalValue<String>?}
  }
}
// @formatter:on
