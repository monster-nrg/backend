package com.monsterenergy.backend.model

import com.monsterenergy.backend.repositories.tables.DoorStatus as TablesDoorStatus

enum class DoorStatus(val status: String) {
  Stored("stored"),
  Moving("moving"),
  Busy("busy");

  fun toEntity() =
    when (this) {
      Stored -> TablesDoorStatus.Stored
      Moving -> TablesDoorStatus.Moving
      Busy -> TablesDoorStatus.Busy
    }

  companion object {
    fun of(entity: TablesDoorStatus) =
      when (entity) {
        TablesDoorStatus.Stored -> Stored
        TablesDoorStatus.Moving -> Moving
        TablesDoorStatus.Busy -> Busy
      }
  }
}
