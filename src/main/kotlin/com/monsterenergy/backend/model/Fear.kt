package com.monsterenergy.backend.model

import java.time.LocalDateTime

// @formatter:off
object Fear {
  object Raw {
    interface ExecutorId { val executorId : Long }
    interface DoorId { val doorId: Long }
  }
  interface Id { val id: Long }
  interface Executor { val executor : User }
  interface Door { val door : Door }
  interface Status { val status : FearStatus }
  interface ExpectedDatetime { val expectedDatetime : LocalDateTime }
  interface StartDatetime { val startDatetime : LocalDateTime? }
  interface FinishDatetime { val finishDatetime : LocalDateTime? }
  interface ExpectedPower { val expectedPower : Float? }
  interface ActualPower { val actualPower : Float? }
  interface Comment { val comment : String? }
}
// @formatter:on
