package com.monsterenergy.backend.model

import java.util.Optional as OptionalValue

// @formatter:off
object Permission {
  interface Id { val id: Long }
  interface Codename {
    val codename: String
    interface Optional { val codename : OptionalValue<String>? }
  }
  interface Name {
    val name: String?
    interface Optional { val name : OptionalValue<String>? }
  }
}
// @formatter:on
