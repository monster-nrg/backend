package com.monsterenergy.backend.model

import java.time.LocalDate
import java.util.Optional as OptionalValue

// @formatter:off
object Child {
  interface Id { val id: Long }
  interface Firstname {
    val firstname: String
    interface Optional { val firstname : OptionalValue<String>? }
  }
  interface Surname {
    val surname: String?
    interface Optional { val surname : OptionalValue<String>? }
  }
  interface Lastname {
    val lastname: String
    interface Optional { val lastname : OptionalValue<String>? }
  }
  interface BirthDate {
    val birthDate: LocalDate
    interface Optional { val birthDate : OptionalValue<LocalDate>? }
  }
  interface Avatar {
    val avatar: String? // Base64
    interface Optional { val avatar : OptionalValue<String>? }
  }
}
// @formatter:on
