package com.monsterenergy.backend.model

import java.util.Optional as OptionalValue

// @formatter:off
object Job {
  object Raw {
    interface DepartmentId {
      val departmentId : Long
      interface Optional { val departmentId : OptionalValue<Long>? }
    }
  }
  interface Id { val id: Long }
  interface Department { val department: Department }
  interface Name {
    val name: String
    interface Optional { val name : OptionalValue<String>? }
  }
}
// @formatter:on
