package com.monsterenergy.backend.services

import com.monsterenergy.backend.dto.door.DoorWithoutIdAndStatus
import com.monsterenergy.backend.services.base.CreateService
import com.monsterenergy.backend.services.base.DeleteService
import com.monsterenergy.backend.services.base.ReadService
import com.monsterenergy.backend.services.base.UpdateService
import com.monsterenergy.backend.repositories.dao.Door as DaoDoor

interface DoorsService :
  CreateService<Long, DoorWithoutIdAndStatus, DaoDoor>,
  ReadService<Long, DaoDoor>,
  UpdateService<Long, DaoDoor>,
  DeleteService<Long>
