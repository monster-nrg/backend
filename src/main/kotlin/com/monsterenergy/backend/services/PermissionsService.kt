package com.monsterenergy.backend.services

import com.monsterenergy.backend.dto.permission.PermissionWithoutId
import com.monsterenergy.backend.repositories.dao.Permission
import com.monsterenergy.backend.services.base.CreateService
import com.monsterenergy.backend.services.base.DeleteService
import com.monsterenergy.backend.services.base.ReadService
import com.monsterenergy.backend.services.base.UpdateService

interface PermissionsService :
  CreateService<Long, PermissionWithoutId, Permission>,
  ReadService<Long, Permission>,
  UpdateService<Long, Permission>,
  DeleteService<Long>
