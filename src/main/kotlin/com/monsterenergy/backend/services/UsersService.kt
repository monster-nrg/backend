package com.monsterenergy.backend.services

import com.monsterenergy.backend.dto.user.UserWithoutId
import com.monsterenergy.backend.repositories.dao.Job
import com.monsterenergy.backend.repositories.dao.User
import com.monsterenergy.backend.services.base.CreateService
import com.monsterenergy.backend.services.base.DeleteService
import com.monsterenergy.backend.services.base.ReadService
import com.monsterenergy.backend.services.base.UpdateService

interface UsersService :
  CreateService<Long, UserWithoutId, User>,
  ReadService<Long, User>,
  UpdateService<Long, User>,
  DeleteService<Long> {
  fun getUserJobs(userId: Long): List<Job>
}
