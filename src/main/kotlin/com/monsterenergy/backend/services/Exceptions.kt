package com.monsterenergy.backend.services

class AuthenticationException : RuntimeException(AuthenticationException::class.java.simpleName)
class AuthorizationException : RuntimeException(AuthorizationException::class.java.simpleName)
