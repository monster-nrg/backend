package com.monsterenergy.backend.services

import com.monsterenergy.backend.dto.department.DepartmentWithoutId
import com.monsterenergy.backend.repositories.dao.Department
import com.monsterenergy.backend.services.base.CreateService
import com.monsterenergy.backend.services.base.DeleteService
import com.monsterenergy.backend.services.base.ReadService
import com.monsterenergy.backend.services.base.UpdateService

interface DepartmentsService :
  CreateService<Long, DepartmentWithoutId, Department>,
  ReadService<Long, Department>,
  UpdateService<Long, Department>,
  DeleteService<Long>
