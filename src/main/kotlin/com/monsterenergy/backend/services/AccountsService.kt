package com.monsterenergy.backend.services

import com.monsterenergy.backend.dto.account.Token
import com.monsterenergy.backend.dto.account.UsernamePassword
import com.monsterenergy.backend.repositories.dao.Account
import com.monsterenergy.backend.services.base.DeleteService
import com.monsterenergy.backend.services.base.ReadService
import com.monsterenergy.backend.services.base.UpdateService
import java.util.*

interface AccountsService :
  ReadService<Long, Account>,
  UpdateService<Long, Account>,
  DeleteService<Long> {
  fun login(credentials: UsernamePassword): Pair<Account, Token>
  fun logout(tokenId: UUID)
  fun getByUsername(username: String): Account
  fun getByTokenId(tokenId: UUID): Account
  fun updateByTokenId(tokenId: UUID, updater: Account.() -> Unit): Account
}
