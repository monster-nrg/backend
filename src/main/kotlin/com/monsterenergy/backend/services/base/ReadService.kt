package com.monsterenergy.backend.services.base

import org.jetbrains.exposed.dao.Entity

interface ReadService<TId : Comparable<TId>, out TEntity : Entity<TId>> {
  fun getAll(): List<TEntity>
  fun getById(id: TId): TEntity
}
