package com.monsterenergy.backend.services.base

import org.jetbrains.exposed.dao.Entity

interface UpdateService<TId : Comparable<TId>, out TEntity : Entity<TId>> {
  fun update(id: TId, updater: TEntity.() -> Unit): TEntity
}
