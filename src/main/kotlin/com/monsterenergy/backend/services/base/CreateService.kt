package com.monsterenergy.backend.services.base

import org.jetbrains.exposed.dao.Entity

interface CreateService<TId : Comparable<TId>, in TModel, out TEntity : Entity<TId>> {
  fun create(model: TModel): TEntity
}
