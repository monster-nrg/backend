package com.monsterenergy.backend.services.base

interface DeleteService<TId : Comparable<TId>> {
  fun deleteById(id: TId)
}
