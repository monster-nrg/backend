package com.monsterenergy.backend.services.impl

import com.monsterenergy.backend.dto.fear.CreateFearRequestDto
import com.monsterenergy.backend.repositories.dao.Fear
import com.monsterenergy.backend.repositories.tables.FearStatus
import com.monsterenergy.backend.services.DoorsService
import com.monsterenergy.backend.services.FearsService
import io.ktor.features.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDateTime

object FearsServiceImpl : FearsService {
  private val usersService: UsersServiceImpl = UsersServiceImpl
  private val doorsService: DoorsService = DoorsServiceImpl

  override fun startFear(id: Long) {
    transaction {
      val fear = getById(id)
      when (fear.status) {
        FearStatus.Planned -> {
          fear.status = FearStatus.InProgress
          fear.startDatetime = LocalDateTime.now()
        }
        FearStatus.InProgress -> throw IllegalStateException("Fear is already in progress")
        FearStatus.Finished -> throw IllegalStateException("Fear cannot be started")
        FearStatus.Cancelled -> throw IllegalStateException("Fear cannot be started")
      }
    }
  }

  override fun finishFear(id: Long, actualPower: Float?) {
    transaction {
      val fear = getById(id)
      when (fear.status) {
        FearStatus.Planned -> throw IllegalStateException("Fear cannot be finished")
        FearStatus.InProgress -> {
          fear.status = FearStatus.Finished
          fear.finishDatetime = LocalDateTime.now()
          fear.actualPower = actualPower
        }
        FearStatus.Finished -> throw IllegalStateException("Fear is already finished")
        FearStatus.Cancelled -> throw IllegalStateException("Fear cannot be finished")
      }
    }
  }

  override fun cancelFear(id: Long) {
    transaction {
      val fear = getById(id)
      when (fear.status) {
        FearStatus.Planned -> {
          fear.status = FearStatus.Cancelled
        }
        FearStatus.InProgress -> throw IllegalStateException("Fear cannot be canceled")
        FearStatus.Finished -> throw IllegalStateException("Fear cannot be canceled")
        FearStatus.Cancelled -> throw IllegalStateException("Fear is already cancelled")
      }
    }
  }

  override fun create(model: CreateFearRequestDto): Fear =
    transaction {
      Fear.new {
        executor = usersService.getById(model.executorId)
        door = doorsService.getById(model.doorId)
        expectedDatetime = model.expectedDatetime
        startDatetime = null
        finishDatetime = null
        expectedPower = model.expectedPower
        comment = model.comment
        status = FearStatus.Planned
      }
    }

  override fun getAll(): List<Fear> =
    transaction {
      Fear.all().toList()
    }

  override fun getById(id: Long): Fear =
    transaction {
      Fear.findById(id) ?: throw BadRequestException("id")
    }
}
