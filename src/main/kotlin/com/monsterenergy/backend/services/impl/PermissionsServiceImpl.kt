package com.monsterenergy.backend.services.impl

import com.monsterenergy.backend.dto.permission.PermissionWithoutId
import com.monsterenergy.backend.services.PermissionsService
import io.ktor.features.*
import org.jetbrains.exposed.sql.transactions.transaction
import com.monsterenergy.backend.repositories.dao.Permission as DaoPermission

object PermissionsServiceImpl : PermissionsService {
  override fun create(model: PermissionWithoutId): DaoPermission = transaction {
    DaoPermission.new {
      this.name = model.name
      this.codename = model.codename
    }
  }

  override fun getAll(): List<DaoPermission> = transaction {
    DaoPermission.all().toList()
  }

  override fun getById(id: Long): DaoPermission = transaction {
    DaoPermission.findById(id) ?: throw BadRequestException("")
  }

  override fun update(id: Long, updater: DaoPermission.() -> Unit): DaoPermission = transaction {
    getById(id).apply(updater).also { it.flush() }
  }

  override fun deleteById(id: Long) = transaction {
    getById(id).delete()
  }
}
