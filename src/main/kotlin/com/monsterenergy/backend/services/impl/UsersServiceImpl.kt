package com.monsterenergy.backend.services.impl

import com.monsterenergy.backend.dto.user.UserWithoutId
import com.monsterenergy.backend.repositories.dao.Job
import com.monsterenergy.backend.repositories.dao.User
import com.monsterenergy.backend.services.UsersService
import com.monsterenergy.backend.util.toExposedBlob
import org.jetbrains.exposed.dao.with
import org.jetbrains.exposed.sql.transactions.transaction

object UsersServiceImpl : UsersService {
  override fun getUserJobs(userId: Long): List<Job> =
    transaction { getById(userId).jobs.with(Job::department).toList() }

  override fun create(model: UserWithoutId): User =
    transaction {
      User.new {
        this.firstname = model.firstname
        this.surname = model.surname
        this.lastname = model.lastname
        this.birthDate = model.birthDate
        this.avatar = model.avatar?.toExposedBlob()
      }
    }

  override fun getAll(): List<User> =
    transaction { User.all().toList() }

  override fun getById(id: Long): User =
    transaction { User[id] }

  override fun update(id: Long, updater: User.() -> Unit): User =
    transaction { getById(id).apply(updater).also(User::flush) }

  override fun deleteById(id: Long) =
    transaction { getById(id).delete() }
}
