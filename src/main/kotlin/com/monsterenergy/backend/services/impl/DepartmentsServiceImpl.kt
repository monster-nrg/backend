package com.monsterenergy.backend.services.impl

import com.monsterenergy.backend.dto.department.DepartmentWithoutId
import com.monsterenergy.backend.repositories.dao.Department
import com.monsterenergy.backend.services.DepartmentsService
import org.jetbrains.exposed.sql.transactions.transaction

object DepartmentsServiceImpl : DepartmentsService {
  override fun create(model: DepartmentWithoutId): Department =
    transaction { Department.new { this.name = model.name } }

  override fun getAll(): List<Department> =
    transaction { Department.all().toList() }

  override fun getById(id: Long): Department =
    transaction { Department[id] }

  override fun update(id: Long, updater: Department.() -> Unit): Department =
    transaction { getById(id).apply(updater).also(Department::flush) }

  override fun deleteById(id: Long) =
    transaction { getById(id).delete() }
}
