package com.monsterenergy.backend.services.impl

import com.monsterenergy.backend.dto.door.DoorWithoutIdAndStatus
import com.monsterenergy.backend.repositories.dao.Door
import com.monsterenergy.backend.repositories.tables.DoorStatus
import com.monsterenergy.backend.services.DoorsService
import com.monsterenergy.backend.util.toExposedBlob
import org.jetbrains.exposed.sql.transactions.transaction

object DoorsServiceImpl : DoorsService {
  override fun create(model: DoorWithoutIdAndStatus): Door =
    transaction {
      Door.new {
        this.name = model.name
        this.description = model.description
        this.status = DoorStatus.Stored
        this.photo = model.photo?.toExposedBlob()
      }
    }

  override fun getAll(): List<Door> =
    transaction { Door.all().toList() }

  override fun getById(id: Long): Door =
    transaction { Door[id] }

  override fun update(id: Long, updater: Door.() -> Unit): Door =
    transaction { getById(id).apply(updater).also(Door::flush) }

  override fun deleteById(id: Long) =
    transaction { getById(id).delete() }
}
