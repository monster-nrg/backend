package com.monsterenergy.backend.services.impl

import com.monsterenergy.backend.dto.job.JobWithoutId
import com.monsterenergy.backend.repositories.dao.Job
import com.monsterenergy.backend.repositories.dao.Permission
import com.monsterenergy.backend.services.DepartmentsService
import com.monsterenergy.backend.services.JobsService
import org.jetbrains.exposed.sql.transactions.transaction

object JobsServiceImpl : JobsService {
  private val departmentsService: DepartmentsService = DepartmentsServiceImpl

  override fun getJobPermissions(id: Long): Set<Permission> =
    transaction {
      getById(id).permissions.toSet()
    }

  override fun create(model: JobWithoutId): Job = transaction {
    Job.new {
      this.name = model.name
      this.department = departmentsService.getById(model.departmentId)
    }
  }

  override fun getAll(): List<Job> =
    transaction { Job.all().toList() }

  override fun getById(id: Long): Job =
    transaction { Job[id] }

  override fun update(id: Long, updater: Job.() -> Unit): Job =
    transaction {
      getById(id).apply(updater).also(Job::flush)
    }

  override fun deleteById(id: Long) =
    transaction { getById(id).delete() }
}
