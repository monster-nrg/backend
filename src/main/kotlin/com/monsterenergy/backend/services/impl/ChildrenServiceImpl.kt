package com.monsterenergy.backend.services.impl

import com.monsterenergy.backend.dto.child.ChildWithoutId
import com.monsterenergy.backend.repositories.dao.Child
import com.monsterenergy.backend.repositories.dao.Door
import com.monsterenergy.backend.services.ChildrenService
import com.monsterenergy.backend.util.toExposedBlob
import io.ktor.features.*
import org.jetbrains.exposed.sql.SizedCollection
import org.jetbrains.exposed.sql.transactions.transaction

object ChildrenServiceImpl : ChildrenService {
  override fun getDoorsByChildId(id: Long): List<Door> = transaction {
    getById(id).doors.toList()
  }

  override fun addDoorToChild(childId: Long, doorId: Long): List<Door> = transaction {
    val doors = getById(childId).doors.toList()
    getById(childId).doors = SizedCollection(doors + DoorsServiceImpl.getById(doorId))
    getDoorsByChildId(childId)
  }

  override fun removeDoorFromChild(childId: Long, doorId: Long): List<Door> = transaction {
    val doors = getById(childId).doors.toList()
    getById(childId).doors =
      SizedCollection(doors.filterNot { it.id.value == DoorsServiceImpl.getById(doorId).id.value })
    getDoorsByChildId(childId)
  }

  override fun create(model: ChildWithoutId): Child = transaction {
    runCatching {
      Child.new {
        this.firstname = model.firstname
        this.surname = model.surname
        this.lastname = model.lastname
        this.birthDate = model.birthDate
        this.avatar = model.avatar?.toExposedBlob()
      }
    }.onFailure { throw BadRequestException("", it) }
      .getOrThrow()
  }

  override fun getAll(): List<Child> = transaction {
    Child.all().toList()
  }

  override fun getById(id: Long): Child = transaction {
    Child.findById(id) ?: throw BadRequestException("")
  }

  override fun update(id: Long, updater: Child.() -> Unit): Child = transaction {
    getById(id).apply(updater).also { it.flush() }
  }

  override fun deleteById(id: Long) = transaction {
    getById(id).delete()
  }
}
