package com.monsterenergy.backend.services.impl

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.monsterenergy.backend.dto.account.Token
import com.monsterenergy.backend.dto.account.UsernamePassword
import com.monsterenergy.backend.repositories.dao.Account
import com.monsterenergy.backend.services.AccountsService
import com.monsterenergy.backend.services.AuthenticationException
import com.monsterenergy.backend.services.JobsService
import com.monsterenergy.backend.services.UsersService
import io.ktor.util.date.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.mindrot.jbcrypt.BCrypt
import java.time.Duration
import java.time.Instant
import java.util.*

object AccountsServiceImpl : AccountsService {
  private val userService: UsersService = UsersServiceImpl
  private val jobService: JobsService = JobsServiceImpl

  override fun login(credentials: UsernamePassword): Pair<Account, Token> =
    transaction {
      val account = getByUsername(credentials.username)
      if (!BCrypt.checkpw(credentials.password, account.password)) {
        throw AuthenticationException()
      }

      val tokenUuid = UUID.randomUUID()
      val userPermissions = userService.getUserJobs(account.id.value)
        .flatMap {
          jobService.getJobPermissions(it.id.value).map { p -> p.codename }
        }

      update(account.id.value) { this.tokenId = tokenUuid }

      val token = JWT.create()
        .withSubject(account.id.value.toString())
        .withClaim("user_id", account.id.value) // TODO: Remove user id from claims
        .withClaim("perm", userPermissions)
        .withJWTId(tokenUuid.toString())
        .withExpiresAt((Instant.now() + Duration.ofHours(24)).toGMTDate().toJvmDate())
        .sign(Algorithm.HMAC256("Saucerful of Secrets"))

      Pair(account, Token(token))
    }

  override fun logout(tokenId: UUID) {
    updateByTokenId(tokenId) { this.tokenId = null }
  }

  override fun getByUsername(username: String): Account =
    transaction { Account.findByUsername(username) ?: TODO("AccountNotFoundException") }

  override fun getByTokenId(tokenId: UUID): Account =
    transaction { Account.findByTokenId(tokenId) ?: TODO("AccountNotFoundException") }

  override fun getAll(): List<Account> =
    transaction { Account.all().toList() }

  override fun getById(id: Long): Account =
    transaction { Account[id] }

  override fun update(id: Long, updater: Account.() -> Unit): Account =
    transaction { getById(id).apply(updater).also(Account::flush) }

  override fun updateByTokenId(tokenId: UUID, updater: Account.() -> Unit): Account =
    transaction { getByTokenId(tokenId).apply(updater).also(Account::flush) }

  override fun deleteById(id: Long) =
    transaction { getById(id).delete() }
}
