package com.monsterenergy.backend.services

import com.monsterenergy.backend.dto.job.JobWithoutId
import com.monsterenergy.backend.repositories.dao.Job
import com.monsterenergy.backend.repositories.dao.Permission
import com.monsterenergy.backend.services.base.CreateService
import com.monsterenergy.backend.services.base.DeleteService
import com.monsterenergy.backend.services.base.ReadService
import com.monsterenergy.backend.services.base.UpdateService

interface JobsService :
  CreateService<Long, JobWithoutId, Job>,
  ReadService<Long, Job>,
  UpdateService<Long, Job>,
  DeleteService<Long> {
  fun getJobPermissions(id: Long): Set<Permission>
}
