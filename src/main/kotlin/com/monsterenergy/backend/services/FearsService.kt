package com.monsterenergy.backend.services

import com.monsterenergy.backend.dto.fear.CreateFearRequestDto
import com.monsterenergy.backend.services.base.CreateService
import com.monsterenergy.backend.services.base.ReadService
import com.monsterenergy.backend.repositories.dao.Fear as DaoFear

interface FearsService :
  CreateService<Long, CreateFearRequestDto, DaoFear>,
  ReadService<Long, DaoFear> {
  fun startFear(id: Long)
  fun finishFear(id: Long, actualPower: Float?)
  fun cancelFear(id: Long)
}
