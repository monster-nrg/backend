package com.monsterenergy.backend.services

import com.monsterenergy.backend.dto.child.ChildWithoutId
import com.monsterenergy.backend.repositories.dao.Child
import com.monsterenergy.backend.repositories.dao.Door
import com.monsterenergy.backend.services.base.CreateService
import com.monsterenergy.backend.services.base.DeleteService
import com.monsterenergy.backend.services.base.ReadService
import com.monsterenergy.backend.services.base.UpdateService

interface ChildrenService :
  CreateService<Long, ChildWithoutId, Child>,
  ReadService<Long, Child>,
  UpdateService<Long, Child>,
  DeleteService<Long> {
  fun getDoorsByChildId(id: Long): List<Door>
  fun addDoorToChild(childId: Long, doorId: Long): List<Door>
  fun removeDoorFromChild(childId: Long, doorId: Long): List<Door>
}
