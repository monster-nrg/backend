package com.monsterenergy.backend

import com.monsterenergy.backend.plugins.*
import com.monsterenergy.backend.repositories.config.DataSource
import com.monsterenergy.backend.repositories.tables.*
import com.monsterenergy.backend.util.fillData
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

fun main(args: Array<String>) {
  DataSource.configureDatasource()
  prepareDb(args.firstOrNull())
  embeddedServer(Netty, port = 8081, host = "0.0.0.0") {
    configureHTTP()
    configureMonitoring()
    configureSecurity()
    configureRouting()
    configureSerialization()
  }.start(wait = true)
}

fun prepareDb(op: String?) {
  val tables = arrayOf(
    Accounts,
    ChildDoors,
    Children,
    Departments,
    Doors,
    Fears,
    JobPermissions,
    Jobs,
    Permissions,
    UserJobs,
    Users
  )
  transaction(DataSource.configureDatasource()) {
    when (val action = op) {
      "drop" -> SchemaUtils.drop(*tables, inBatch = true)
      "fill" -> fillData()
      null -> SchemaUtils.createMissingTablesAndColumns(*tables, inBatch = true)
      else -> println("Wrong action '$action'")
    }
  }
}
