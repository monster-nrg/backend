package com.monsterenergy.backend.util

import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.google.gson.TypeAdapterFactory
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import java.lang.reflect.ParameterizedType
import java.util.*

class OptionalTypeAdapter<T>(private val contentAdapter: TypeAdapter<T>) : TypeAdapter<Optional<T>>() {
  override fun write(writer: JsonWriter, value: Optional<T>?) {
    fun writeNullOrUndefined(isNull: Boolean) {
      val originalSerializeNulls = writer.serializeNulls
      writer.serializeNulls = isNull
      writer.nullValue()
      writer.serializeNulls = originalSerializeNulls
    }

    fun writeNull() = writeNullOrUndefined(true)
    fun writeUndefined() = writeNullOrUndefined(false)

    when {
      value == null -> writeUndefined()
      value.isPresent -> contentAdapter.write(writer, value.get())
      else -> writeNull()
    }
  }

  override fun read(reader: JsonReader): Optional<T> =
    reader.takeIf { it.peek() != JsonToken.NULL }
      ?.let { Optional.ofNullable(contentAdapter.read(reader)) }
      ?: Optional.empty<T>().also { reader.nextNull() }

  companion object : TypeAdapterFactory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : Any?> create(gson: Gson, type: TypeToken<T>): TypeAdapter<T>? =
      when (type.rawType) {
        Optional::class.java -> {
          (type.type as? ParameterizedType)?.actualTypeArguments?.firstOrNull()
            ?.let { gson.getAdapter(TypeToken.get(it)) }
            ?.let { OptionalTypeAdapter(it) as? TypeAdapter<T> }
        }
        else -> null
      }
  }
}
