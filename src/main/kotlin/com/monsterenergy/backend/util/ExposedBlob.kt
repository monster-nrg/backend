package com.monsterenergy.backend.util

import org.jetbrains.exposed.sql.statements.api.ExposedBlob
import java.util.*

fun ExposedBlob.toBase64String(): String =
  Base64.getEncoder().encodeToString(this.bytes)

fun String.toExposedBlob(): ExposedBlob =
  ExposedBlob(Base64.getDecoder().decode(this))
