package com.monsterenergy.backend.util

abstract class ApiResponse(val status: String)

data class SuccessResponse<P>(val payload: P) : ApiResponse("success")
data class ErrorResponse<E>(val error: E) : ApiResponse("error")
