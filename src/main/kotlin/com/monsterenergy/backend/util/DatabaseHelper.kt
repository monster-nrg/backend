package com.monsterenergy.backend.util

import com.monsterenergy.backend.repositories.config.DataSource
import com.monsterenergy.backend.repositories.dao.Account
import com.monsterenergy.backend.repositories.dao.User
import com.monsterenergy.backend.repositories.tables.*
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.transactions.transaction
import org.mindrot.jbcrypt.BCrypt
import java.time.LocalDate

fun main(args: Array<String>) {
  val tables = arrayOf(
    Accounts,
    ChildDoors,
    Children,
    Departments,
    Doors,
    Fears,
    JobPermissions,
    Jobs,
    Permissions,
    UserJobs,
    Users
  )
  transaction(DataSource.configureDatasource()) {
    when (val action = args.firstOrNull()) {
      "drop" -> SchemaUtils.drop(*tables, inBatch = true)
      "fill" -> fillData()
      null -> SchemaUtils.createMissingTablesAndColumns(*tables, inBatch = true)
      else -> println("Wrong action '$action'")
    }
  }
}

fun Transaction.fillData() {
  Account.new {
    user = User.new {
      firstname = "Майкл"
      surname = "Петрович"
      lastname = "Вазовски"
      birthDate = LocalDate.of(1990, 1, 5)
    }
    username = "wazzowski"
    password = BCrypt.hashpw("password", BCrypt.gensalt())
  }
}
