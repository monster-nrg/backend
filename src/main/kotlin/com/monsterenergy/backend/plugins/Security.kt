package com.monsterenergy.backend.plugins

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.monsterenergy.backend.plugins.autorization.StringPermissionAuthorization
import com.monsterenergy.backend.repositories.dao.Account
import com.monsterenergy.backend.services.AuthenticationException
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*

fun Application.configureSecurity() {
  authentication {
    jwt {
      verifier(
        JWT.require(Algorithm.HMAC256("Saucerful of Secrets"))
          .build()
      )
      challenge { _, _ -> throw AuthenticationException() }
      validate { credential ->
        transaction {
          (credential.payload.id)
            ?.let(UUID::fromString)
            ?.let(Account::findByTokenId)
            ?.let { JWTPrincipal(credential.payload) }
        }
      }
    }
  }
  install(StringPermissionAuthorization) {
    extractPermissions {
      when (it) {
        is JWTPrincipal -> it.getListClaim("perm", String::class).toSet()
        else -> emptySet()
      }
    }
  }
}
