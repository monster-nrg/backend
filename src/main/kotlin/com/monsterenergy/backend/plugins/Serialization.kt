package com.monsterenergy.backend.plugins

import com.monsterenergy.backend.util.OptionalTypeAdapter
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.gson.*

fun Application.configureSerialization() {
  install(ContentNegotiation) {
    gson {
      disableHtmlEscaping()
      setPrettyPrinting()
      serializeNulls()
      registerTypeAdapterFactory(OptionalTypeAdapter)
    }
  }
}
