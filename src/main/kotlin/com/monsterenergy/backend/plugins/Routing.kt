package com.monsterenergy.backend.plugins

import com.google.gson.JsonParseException
import com.monsterenergy.backend.controllers.accounts.AccountsController
import com.monsterenergy.backend.controllers.children.ChildrenController
import com.monsterenergy.backend.controllers.departments.DepartmentsController
import com.monsterenergy.backend.controllers.doors.DoorsController
import com.monsterenergy.backend.controllers.fears.FearsController
import com.monsterenergy.backend.controllers.jobs.JobsController
import com.monsterenergy.backend.controllers.permissions.PermissionsController
import com.monsterenergy.backend.controllers.users.UsersController
import com.monsterenergy.backend.services.AuthenticationException
import com.monsterenergy.backend.services.AuthorizationException
import com.monsterenergy.backend.services.impl.*
import com.monsterenergy.backend.util.ErrorResponse
import com.monsterenergy.backend.util.SuccessResponse
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.locations.*
import io.ktor.response.*
import io.ktor.routing.*

fun Application.configureRouting() {
  install(AutoHeadResponse)
  install(Locations)

  routing {
    route("/api") {
      sendPipeline.intercept(ApplicationSendPipeline.Transform) {
        if (subject is OutgoingContent || subject is HttpStatusCode)
          return@intercept

        if (context.response.status()?.isSuccess() == false) {
          proceedWith(ErrorResponse(subject))
        } else {
          proceedWith(SuccessResponse(subject))
        }
      }
      AccountsController(AccountsServiceImpl).mount(this)
      ChildrenController(ChildrenServiceImpl).mount(this)
      DepartmentsController(DepartmentsServiceImpl).mount(this)
      DoorsController(DoorsServiceImpl).mount(this)
      FearsController(FearsServiceImpl).mount(this)
      JobsController(JobsServiceImpl, DepartmentsServiceImpl).mount(this)
      UsersController(UsersServiceImpl).mount(this)
      PermissionsController(PermissionsServiceImpl).mount(this)
    }

    static("/") {
      resources("static")
    }

    install(StatusPages) {
      exception<AuthenticationException> { cause ->
        call.respond(HttpStatusCode.Unauthorized, mapOf("message" to cause.message))
      }
      exception<AuthorizationException> { cause ->
        call.respond(HttpStatusCode.Forbidden, mapOf("message" to cause.message))
      }
      exception<JsonParseException> { cause ->
        call.respond(HttpStatusCode.BadRequest, mapOf("message" to cause.message))
      }
      exception<Exception> { cause ->
        call.respond(HttpStatusCode.BadRequest, mapOf("message" to cause.message))
      }
    }
  }
}
