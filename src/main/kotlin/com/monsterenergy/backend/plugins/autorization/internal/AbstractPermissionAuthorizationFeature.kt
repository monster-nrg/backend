package com.monsterenergy.backend.plugins.autorization.internal

import io.ktor.application.*
import kotlin.reflect.KClass

abstract class AbstractPermissionAuthorizationFeature<TPermission : Any>(
  private val permissionClass: KClass<TPermission>
) : ApplicationFeature<
  ApplicationCallPipeline,
  Configuration<TPermission>,
  PermissionAuthorizationInterceptor<TPermission>> {
  protected object FeatureRegistry : HashMap<KClass<*>, AbstractPermissionAuthorizationFeature<*>>()
  companion object {
    @Suppress("UNCHECKED_CAST")
    fun <TPermission : Any> getFeatureByPermissionClass(permissionClass: KClass<in TPermission>):
      AbstractPermissionAuthorizationFeature<TPermission> =
      FeatureRegistry.getValue(permissionClass) as AbstractPermissionAuthorizationFeature<TPermission>
  }


  override fun install(
    pipeline: ApplicationCallPipeline,
    configure: Configuration<TPermission>.() -> Unit
  ): PermissionAuthorizationInterceptor<TPermission> {
    FeatureRegistry.putIfAbsent(permissionClass, this)
    return PermissionAuthorizationInterceptor(Configuration<TPermission>().apply(configure))
  }
}

