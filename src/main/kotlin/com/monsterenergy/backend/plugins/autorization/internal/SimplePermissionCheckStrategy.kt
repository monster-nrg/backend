package com.monsterenergy.backend.plugins.autorization.internal

import com.monsterenergy.backend.services.AuthorizationException

class SimplePermissionCheckStrategy<in TPermission>(
  private val permission: TPermission
) : PermissionCheckStrategy<TPermission> {
  override fun check(permissions: Set<TPermission>) {
    if (!permissions.contains(permission)) throw AuthorizationException()
  }
}
