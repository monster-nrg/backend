package com.monsterenergy.backend.plugins.autorization.internal

import io.ktor.routing.*

class AuthorizedRouteSelector(private val description: String) : RouteSelector() {
  override fun evaluate(context: RoutingResolveContext, segmentIndex: Int): RouteSelectorEvaluation =
    RouteSelectorEvaluation.Constant

  override fun toString(): String = "(authorize ${description})"
}
