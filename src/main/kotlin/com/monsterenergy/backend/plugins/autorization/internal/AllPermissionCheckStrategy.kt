package com.monsterenergy.backend.plugins.autorization.internal

import com.monsterenergy.backend.services.AuthorizationException

class AllPermissionCheckStrategy<in TPermission>(
  private val requiredPermissions: Set<TPermission>
) : PermissionCheckStrategy<TPermission> {
  override fun check(permissions: Set<TPermission>) {
    if (!permissions.containsAll(requiredPermissions)) throw AuthorizationException()
  }
}
