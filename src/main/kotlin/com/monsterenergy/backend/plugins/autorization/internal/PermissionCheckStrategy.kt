package com.monsterenergy.backend.plugins.autorization.internal

interface PermissionCheckStrategy<in TPermission> {
  fun check(permissions: Set<TPermission>)
}
