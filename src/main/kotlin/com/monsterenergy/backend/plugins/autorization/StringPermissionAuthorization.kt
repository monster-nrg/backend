package com.monsterenergy.backend.plugins.autorization

import com.monsterenergy.backend.plugins.autorization.internal.AbstractPermissionAuthorizationFeature
import com.monsterenergy.backend.plugins.autorization.internal.PermissionAuthorizationInterceptor
import io.ktor.util.*

object StringPermissionAuthorization : AbstractPermissionAuthorizationFeature<String>(String::class) {
  override val key = AttributeKey<PermissionAuthorizationInterceptor<String>>("StringPermissionAuthorization")
}
