package com.monsterenergy.backend.plugins.autorization.internal

import io.ktor.auth.*

class Configuration<TPermission> {
  internal var permissionExtractor: (Principal) -> Set<TPermission> = { emptySet() }
  fun extractPermissions(permissionExtractor: (Principal) -> Set<TPermission>) {
    this.permissionExtractor = permissionExtractor
  }
}
