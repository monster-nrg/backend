package com.monsterenergy.backend.plugins.autorization

import com.monsterenergy.backend.plugins.autorization.internal.*
import io.ktor.application.*
import io.ktor.routing.*

inline fun <reified TPermission> Route.authorizeRoute(
  strategy: PermissionCheckStrategy<TPermission>,
  routeBuilder: Route.() -> Unit
): Route {
  val authorizedRoute = createChild(AuthorizedRouteSelector(""))
  application.feature(
    AbstractPermissionAuthorizationFeature
      .getFeatureByPermissionClass(TPermission::class)
  ).interceptPipeline(authorizedRoute, strategy)
  authorizedRoute.routeBuilder()
  return authorizedRoute
}

inline fun <reified TPermission> Route.withPermission(
  permission: TPermission, routeBuilder: Route.() -> Unit
) = authorizeRoute(SimplePermissionCheckStrategy(permission), routeBuilder)

inline fun <reified TPermission> Route.withAllPermissions(
  vararg permission: TPermission,
  routeBuilder: Route.() -> Unit
) = authorizeRoute(AllPermissionCheckStrategy(setOf(*permission)), routeBuilder)

inline fun <reified TPermission> Route.withAnyPermission(
  vararg permission: TPermission,
  routeBuilder: Route.() -> Unit
) = authorizeRoute(AnyPermissionCheckStrategy(setOf(*permission)), routeBuilder)

inline fun <reified TPermission> Route.withoutPermission(
  vararg permission: TPermission,
  routeBuilder: Route.() -> Unit
) = authorizeRoute(NonePermissionCheckStrategy(setOf(*permission)), routeBuilder)
