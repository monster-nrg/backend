package com.monsterenergy.backend.plugins.autorization.internal

import com.monsterenergy.backend.services.AuthorizationException
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.util.pipeline.*

class PermissionAuthorizationInterceptor<TPermission>(
  private val configuration: Configuration<TPermission>
) {
  private companion object {
    val AuthorizationPhase = PipelinePhase("Authorization")
  }

  fun interceptPipeline(
    pipeline: ApplicationCallPipeline,
    checkStrategy: PermissionCheckStrategy<TPermission>
  ) {
    pipeline.insertPhaseAfter(ApplicationCallPipeline.Features, Authentication.ChallengePhase)
    pipeline.insertPhaseAfter(Authentication.ChallengePhase, AuthorizationPhase)
    pipeline.intercept(AuthorizationPhase) {
      val principal = call.authentication.principal<Principal>() ?: throw AuthorizationException()
      val permissions = configuration.permissionExtractor(principal)
      checkStrategy.check(permissions)
    }
  }
}
