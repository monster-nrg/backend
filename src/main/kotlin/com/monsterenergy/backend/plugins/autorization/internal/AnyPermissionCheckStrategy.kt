package com.monsterenergy.backend.plugins.autorization.internal

import com.monsterenergy.backend.services.AuthorizationException

class AnyPermissionCheckStrategy<in TPermission>(
  private val requiredPermissions: Set<TPermission>
) : PermissionCheckStrategy<TPermission> {
  override fun check(permissions: Set<TPermission>) {
    if (!requiredPermissions.any { permissions.contains(it) }) throw AuthorizationException()
  }
}

