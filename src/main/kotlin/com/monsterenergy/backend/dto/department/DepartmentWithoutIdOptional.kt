package com.monsterenergy.backend.dto.department

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Department
import java.util.*

@Dto(forModel = Department::class)
data class DepartmentWithoutIdOptional(
  override val name: Optional<String>? = null
) : Department.Name.Optional {
  companion object {
    fun of(entity: com.monsterenergy.backend.repositories.dao.Department) =
      DepartmentWithoutIdOptional(
        Optional.ofNullable(entity.name)
      )
  }
}
