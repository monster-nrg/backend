package com.monsterenergy.backend.dto.department

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Department
import com.monsterenergy.backend.repositories.dao.Department as DaoDepartment

@Dto(forModel = Department::class)
data class DepartmentId(
  override val id: Long
) : Department.Id {
  companion object {
    fun of(entity: DaoDepartment) =
      DepartmentId(entity.id.value)
  }
}
