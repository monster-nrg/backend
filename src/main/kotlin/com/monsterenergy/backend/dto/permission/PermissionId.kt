package com.monsterenergy.backend.dto.permission

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Permission
import com.monsterenergy.backend.repositories.dao.Permission as DaoPermission

@Dto(forModel = Permission::class)
data class PermissionId(
  override val id: Long,
) : Permission.Id {
  companion object {
    fun of(entity: DaoPermission) =
      PermissionId(entity.id.value)
  }
}
