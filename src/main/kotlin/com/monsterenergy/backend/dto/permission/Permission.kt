package com.monsterenergy.backend.dto.permission

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Permission
import com.monsterenergy.backend.repositories.dao.Permission as DaoPermission

@Dto(forModel = Permission::class)
data class Permission(
  override val id: Long,
  override val codename: String,
  override val name: String?
) : Permission.Id,
  Permission.Codename,
  Permission.Name {
  companion object {
    fun of(entity: DaoPermission) =
      Permission(
        entity.id.value,
        entity.codename,
        entity.name
      )
  }
}
