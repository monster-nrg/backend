package com.monsterenergy.backend.dto.permission

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Permission

@Dto(forModel = Permission::class)
data class PermissionWithoutId(
  override val codename: String,
  override val name: String?
) : Permission.Codename,
  Permission.Name
