package com.monsterenergy.backend.dto.permission

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Permission
import java.util.*

@Dto(forModel = Permission::class)
data class PermissionWithoutIdOptional(
  override val codename: Optional<String>?,
  override val name: Optional<String>?
) : Permission.Codename.Optional,
  Permission.Name.Optional
