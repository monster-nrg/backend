package com.monsterenergy.backend.dto.fear

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Fear
import com.monsterenergy.backend.model.FearStatus
import java.time.LocalDateTime
import com.monsterenergy.backend.repositories.dao.Fear as FearDao

@Dto(forModel = Fear::class)
data class Fear(
  override val id: Long,
  override val executorId: Long,
  override val doorId: Long,
  override val status: FearStatus,
  override val expectedDatetime: LocalDateTime,
  override val startDatetime: LocalDateTime?,
  override val finishDatetime: LocalDateTime?,
  override val expectedPower: Float?,
  override val actualPower: Float?,
  override val comment: String?
) : Fear.Id,
  Fear.Raw.ExecutorId,
  Fear.Raw.DoorId,
  Fear.Status,
  Fear.ExpectedDatetime,
  Fear.StartDatetime,
  Fear.FinishDatetime,
  Fear.ExpectedPower,
  Fear.ActualPower,
  Fear.Comment {
  companion object {
    fun of(entity: FearDao) = Fear(
      entity.id.value,
      entity.executorId.value,
      entity.doorId.value,
      FearStatus.of(entity.status),
      entity.expectedDatetime,
      entity.startDatetime,
      entity.finishDatetime,
      entity.expectedPower,
      entity.actualPower,
      entity.comment
    )
  }
}
