package com.monsterenergy.backend.dto.fear

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Fear

@Dto(forModel = Fear::class)
data class FearFinishRequest(
  override val actualPower: Float?
) : Fear.ActualPower
