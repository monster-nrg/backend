package com.monsterenergy.backend.dto.fear

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Fear
import java.time.LocalDateTime

@Dto(forModel = Fear::class)
data class CreateFearRequestDto(
  override val executorId: Long,
  override val doorId: Long,
  override val expectedDatetime: LocalDateTime,
  override val expectedPower: Float?,
  override val comment: String?
) : Fear.Raw.ExecutorId,
  Fear.Raw.DoorId,
  Fear.ExpectedDatetime,
  Fear.ExpectedPower,
  Fear.Comment
