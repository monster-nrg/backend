package com.monsterenergy.backend.dto.child

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Child
import com.monsterenergy.backend.repositories.dao.Child as DaoChild

@Dto(forModel = Child::class)
data class ChildId(
  override val id: Long,
) : Child.Id {
  companion object {
    fun of(entity: DaoChild) = ChildId(entity.id.value)
  }
}
