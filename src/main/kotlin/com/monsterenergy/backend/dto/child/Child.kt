package com.monsterenergy.backend.dto.child

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Child
import com.monsterenergy.backend.util.toBase64String
import java.time.LocalDate
import com.monsterenergy.backend.repositories.dao.Child as DaoChild


@Dto(forModel = Child::class)
data class Child(
  override val id: Long,
  override val firstname: String,
  override val surname: String? = null,
  override val lastname: String,
  override val birthDate: LocalDate,
  override val avatar: String? = null
) : Child.Id,
  Child.Firstname,
  Child.Surname,
  Child.Lastname,
  Child.BirthDate,
  Child.Avatar {
  companion object {
    fun of(entity: DaoChild) = Child(
      entity.id.value,
      entity.firstname,
      entity.surname,
      entity.lastname,
      entity.birthDate,
      entity.avatar?.toBase64String()
    )
  }
}

