package com.monsterenergy.backend.dto.child

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Child
import com.monsterenergy.backend.util.toBase64String
import java.time.LocalDate
import java.util.*
import com.monsterenergy.backend.repositories.dao.Child as DaoChild

@Dto(forModel = Child::class)
data class ChildWithoutIdOptional(
  override val firstname: Optional<String>? = null,
  override val surname: Optional<String>? = null,
  override val lastname: Optional<String>? = null,
  override val birthDate: Optional<LocalDate>? = null,
  override val avatar: Optional<String>? = null
) : Child.Firstname.Optional,
  Child.Surname.Optional,
  Child.Lastname.Optional,
  Child.BirthDate.Optional,
  Child.Avatar.Optional {
  companion object {
    fun of(entity: DaoChild) = ChildWithoutIdOptional(
      Optional.ofNullable(entity.firstname),
      Optional.ofNullable(entity.surname),
      Optional.ofNullable(entity.lastname),
      Optional.ofNullable(entity.birthDate),
      Optional.ofNullable(entity.avatar?.toBase64String())
    )
  }
}
