package com.monsterenergy.backend.dto.user

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.User
import com.monsterenergy.backend.util.toBase64String
import java.time.LocalDate
import com.monsterenergy.backend.repositories.dao.User as DaoUser


@Dto(forModel = User::class)
data class User(
  override val id: Long,
  override val firstname: String,
  override val surname: String? = null,
  override val lastname: String,
  override val birthDate: LocalDate,
  override val avatar: String? = null
) : User.Id,
  User.Firstname,
  User.Surname,
  User.Lastname,
  User.BirthDate,
  User.Avatar {
  companion object {
    fun of(entity: DaoUser) = User(
      entity.id.value,
      entity.firstname,
      entity.surname,
      entity.lastname,
      entity.birthDate,
      entity.avatar?.toBase64String()
    )
  }
}
