package com.monsterenergy.backend.dto.user

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.User
import com.monsterenergy.backend.util.toBase64String
import java.time.LocalDate
import java.util.*
import com.monsterenergy.backend.repositories.dao.User as DaoUser

@Dto(forModel = User::class)
data class UserWithoutIdOptional(
  override val firstname: Optional<String>? = null,
  override val surname: Optional<String>? = null,
  override val lastname: Optional<String>? = null,
  override val birthDate: Optional<LocalDate>? = null,
  override val avatar: Optional<String>? = null
) : User.Firstname.Optional,
  User.Surname.Optional,
  User.Lastname.Optional,
  User.BirthDate.Optional,
  User.Avatar.Optional {
  companion object {
    fun of(entity: DaoUser) = UserWithoutIdOptional(
      Optional.ofNullable(entity.firstname),
      Optional.ofNullable(entity.surname),
      Optional.ofNullable(entity.lastname),
      Optional.ofNullable(entity.birthDate),
      Optional.ofNullable(entity.avatar?.toBase64String())
    )
  }
}
