package com.monsterenergy.backend.dto.account

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Account

@Dto(forModel = Account::class)
data class UsernamePassword(
  override val username: String,
  override val password: String
) : Account.Username,
  Account.Password {
    companion object {
      fun of(entity: com.monsterenergy.backend.repositories.dao.Account) = UsernamePassword(
        entity.username,
        entity.password
      )
    }
  }

