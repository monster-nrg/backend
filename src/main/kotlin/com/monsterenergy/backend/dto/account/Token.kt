package com.monsterenergy.backend.dto.account

data class Token(val token: String)
