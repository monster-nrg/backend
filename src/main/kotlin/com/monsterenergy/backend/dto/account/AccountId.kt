package com.monsterenergy.backend.dto.account

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Account

@Dto(forModel = Account::class)
data class AccountId(
  override val id: Long
) : Account.Id

