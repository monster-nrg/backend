package com.monsterenergy.backend.dto.account

data class LoginSuccess(
  val account: IdUsername,
  val token: Token
)
