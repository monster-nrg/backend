package com.monsterenergy.backend.dto.account

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Account
import java.util.*

@Dto(forModel = Account::class)
data class UsernamePasswordOptional(
  override val username: Optional<String>? = null,
  override val password: Optional<String>? = null,
) : Account.Username.Optional,
  Account.Password.Optional
