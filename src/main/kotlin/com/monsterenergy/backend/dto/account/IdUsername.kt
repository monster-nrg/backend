package com.monsterenergy.backend.dto.account

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Account

@Dto(forModel = Account::class)
data class IdUsername(
  override val id: Long,
  override val username: String
) : Account.Id,
  Account.Username {
  companion object {
    fun of(entity: com.monsterenergy.backend.repositories.dao.Account) = IdUsername(
      entity.id.value,
      entity.username,
    )
  }
}
