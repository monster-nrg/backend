package com.monsterenergy.backend.dto.job

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Job

@Dto(forModel = Job::class)
data class JobWithoutId(
  override val departmentId: Long,
  override val name: String
) : Job.Name,
  Job.Raw.DepartmentId

