package com.monsterenergy.backend.dto.job

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Job
import java.util.*

@Dto(forModel = Job::class)
data class JobWithoutIdOptional(
  override val departmentId: Optional<Long>?,
  override val name: Optional<String>?
) : Job.Name.Optional,
  Job.Raw.DepartmentId.Optional
