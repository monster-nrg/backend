package com.monsterenergy.backend.dto.job

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Job
import com.monsterenergy.backend.repositories.dao.Job as DaoJob

@Dto(forModel = Job::class)
data class JobId(
  override val id: Long
) : Job.Id {
  companion object {
    fun of(entity: DaoJob) =
      JobId(entity.id.value)
  }
}

