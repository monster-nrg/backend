package com.monsterenergy.backend.dto.job

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Job
import com.monsterenergy.backend.repositories.dao.Job as DaoJob

@Dto(forModel = Job::class)
data class Job(
  override val id: Long,
  override val name: String,
  override val departmentId: Long
) : Job.Id,
  Job.Name,
  Job.Raw.DepartmentId {
  companion object {
    fun of(entity: DaoJob) =
      Job(
        entity.id.value,
        entity.name,
        entity.departmentId.value
      )
  }
}
