package com.monsterenergy.backend.dto

import kotlin.reflect.KClass

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.SOURCE)
annotation class Dto(
  val forModel: KClass<*>
)
