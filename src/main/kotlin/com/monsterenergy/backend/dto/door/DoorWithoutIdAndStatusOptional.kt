package com.monsterenergy.backend.dto.door

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Door
import com.monsterenergy.backend.util.toBase64String
import java.util.*
import com.monsterenergy.backend.repositories.dao.Door as DaoDoor

@Dto(forModel = Door::class)
data class DoorWithoutIdAndStatusOptional(
  override val name: Optional<String>?,
  override val description: Optional<String>?,
  override val photo: Optional<String>?
) : Door.Name.Optional,
  Door.Description.Optional,
  Door.Photo.Optional {
  companion object {
    fun of(entity: DaoDoor) = DoorWithoutIdAndStatusOptional(
      Optional.ofNullable(entity.name),
      Optional.ofNullable(entity.description),
      Optional.ofNullable(entity.photo?.toBase64String())
    )
  }
}
