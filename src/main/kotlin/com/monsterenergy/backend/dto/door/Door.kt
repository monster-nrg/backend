package com.monsterenergy.backend.dto.door

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Door
import com.monsterenergy.backend.model.DoorStatus
import com.monsterenergy.backend.util.toBase64String
import com.monsterenergy.backend.repositories.dao.Door as DaoDoor

@Dto(forModel = Door::class)
data class Door(
  override val id: Long,
  override val status: DoorStatus,
  override val name: String,
  override val description: String?,
  override val photo: String?
) : Door.Id,
  Door.Name,
  Door.Description,
  Door.Status,
  Door.Photo {
  companion object {
    fun of(entity: DaoDoor) = Door(
      entity.id.value,
      DoorStatus.of(entity.status),
      entity.name,
      entity.description,
      entity.photo?.toBase64String()
    )
  }
}

