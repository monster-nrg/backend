package com.monsterenergy.backend.dto.door

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Door
import com.monsterenergy.backend.util.toBase64String
import com.monsterenergy.backend.repositories.dao.Door as DaoDoor

@Dto(forModel = Door::class)
data class DoorWithoutIdAndStatus(
  override val name: String,
  override val description: String?,
  override val photo: String?
) : Door.Name,
  Door.Description,
  Door.Photo {
  companion object {
    fun of(entity: DaoDoor) = DoorWithoutIdAndStatus(
      entity.name,
      entity.description,
      entity.photo?.toBase64String()
    )
  }
}
