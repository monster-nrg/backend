package com.monsterenergy.backend.dto.door

import com.monsterenergy.backend.dto.Dto
import com.monsterenergy.backend.model.Door
import com.monsterenergy.backend.repositories.dao.Door as DaoDoor

@Dto(forModel = Door::class)
data class DoorId(
  override val id: Long,
) : Door.Id {
  companion object {
    fun of(entity: DaoDoor) = DoorId(
      entity.id.value
    )
  }
}
