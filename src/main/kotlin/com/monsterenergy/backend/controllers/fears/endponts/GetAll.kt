package com.monsterenergy.backend.controllers.fears.endponts

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.fears.FearsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.fear.Fear

class GetAll(controller: FearsController) :
  Endpoint<FearsController, Unit, List<Fear>>
    (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): List<Fear> {
    return controller.fearsService.getAll().map(Fear::of)
  }
}
