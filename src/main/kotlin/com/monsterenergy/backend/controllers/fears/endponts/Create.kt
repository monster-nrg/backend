package com.monsterenergy.backend.controllers.fears.endponts

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.fears.FearsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.fear.CreateFearRequestDto
import com.monsterenergy.backend.dto.fear.Fear

class Create(
  controller: FearsController,
) : Endpoint<FearsController, CreateFearRequestDto, Fear>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: CreateFearRequestDto): Fear {
    return controller.fearsService.create(requestBody).let(Fear::of)
  }
}
