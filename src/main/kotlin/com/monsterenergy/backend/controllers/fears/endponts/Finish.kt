package com.monsterenergy.backend.controllers.fears.endponts

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.fears.FearsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.fear.FearFinishRequest

class Finish(
  controller: FearsController,
  private val idSupplier: Context.() -> Long
) : Endpoint<FearsController, FearFinishRequest, Unit>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: FearFinishRequest) {
    return controller.fearsService.finishFear(idSupplier(), requestBody.actualPower)
  }
}
