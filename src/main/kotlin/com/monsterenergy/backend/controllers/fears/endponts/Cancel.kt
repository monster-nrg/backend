package com.monsterenergy.backend.controllers.fears.endponts

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.fears.FearsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass

class Cancel(
  controller: FearsController,
  private val idSupplier: Context.() -> Long
) : Endpoint<FearsController, Unit, Unit>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit) {
    return controller.fearsService.cancelFear(idSupplier())
  }
}
