package com.monsterenergy.backend.controllers.fears.endponts

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.fears.FearsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.fear.Fear

class GetById(
  controller: FearsController,
  private val idSupplier: Context.() -> Long
) : Endpoint<FearsController, Unit, Fear>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): Fear {
    return controller.fearsService.getById(idSupplier()).let(Fear::of)
  }
}
