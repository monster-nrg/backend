package com.monsterenergy.backend.controllers.fears

import com.monsterenergy.backend.controllers.base.Controller
import com.monsterenergy.backend.controllers.fears.endponts.*
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.services.FearsService
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.routing.*
import io.ktor.util.*

class FearsController(
  internal val fearsService: FearsService
) : Controller {
  private companion object {
    const val id = "Id"

    fun Context.getFearId() =
      call.parameters.getOrFail<Long>(id)
  }
  override fun mount(parentRoute: Route) {
    parentRoute.apply {
      authenticate {
        route("/fears") {
          post(Create(this@FearsController))
          get(GetAll(this@FearsController))
          route("/{$id}") {
            get(GetById(this@FearsController) { getFearId() })
            post("/start", Start(this@FearsController) { getFearId() })
            post("/finish", Finish(this@FearsController) { getFearId() })
            post("/cancel", Cancel(this@FearsController) { getFearId() })
          }
        }
      }
    }
  }
}
