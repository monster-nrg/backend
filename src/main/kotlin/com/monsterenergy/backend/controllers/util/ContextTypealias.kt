package com.monsterenergy.backend.controllers.util

import io.ktor.application.*
import io.ktor.util.pipeline.*

typealias Context = PipelineContext<Unit, ApplicationCall>
