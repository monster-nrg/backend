package com.monsterenergy.backend.controllers.util

import com.monsterenergy.backend.services.AuthorizationException
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*

object ControllerHelpers {
  fun Context.getJwtPrincipal(): JWTPrincipal =
    call.principal() ?: throw AuthorizationException()

  fun JWTPrincipal.getUserPermissions(): List<String> =
    this.getListClaim("perm", String::class)
}

