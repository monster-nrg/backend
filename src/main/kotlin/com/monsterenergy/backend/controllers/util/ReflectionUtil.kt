package com.monsterenergy.backend.controllers.util

import kotlin.reflect.KClass

inline fun <reified K : Any> getKClass(): KClass<K> = K::class
