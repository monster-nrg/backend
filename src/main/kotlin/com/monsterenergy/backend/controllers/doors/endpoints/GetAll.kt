package com.monsterenergy.backend.controllers.doors.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.doors.DoorsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.door.Door

class GetAll(controller: DoorsController) :
  Endpoint<DoorsController, Unit, List<Door>>
    (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): List<Door> {
    return controller.doorsService.getAll().map(Door::of)
  }
}
