package com.monsterenergy.backend.controllers.doors.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.doors.DoorsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.door.Door
import com.monsterenergy.backend.dto.door.DoorWithoutIdAndStatusOptional
import com.monsterenergy.backend.util.toExposedBlob
import io.ktor.features.*

class Update(
  controller: DoorsController,
  private val idSupplier: Context.() -> Long
) : Endpoint<DoorsController, DoorWithoutIdAndStatusOptional, Door>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: DoorWithoutIdAndStatusOptional): Door {
    try {
      return controller.doorsService.update(idSupplier()) {
        requestBody.name?.let { this.name = it.orElse(null) }
        requestBody.description?.let { this.description = it.orElse(null) }
        requestBody.photo?.let { this.photo = it.map(String::toExposedBlob).orElse(null) }
      }.let(Door::of)
    } catch (npe: NullPointerException) {
      throw BadRequestException("NPE", npe)
    }
  }
}
