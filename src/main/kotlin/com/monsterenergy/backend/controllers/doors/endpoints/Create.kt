package com.monsterenergy.backend.controllers.doors.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.doors.DoorsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.door.DoorId
import com.monsterenergy.backend.dto.door.DoorWithoutIdAndStatus

class Create(controller: DoorsController) :
  Endpoint<DoorsController, DoorWithoutIdAndStatus, DoorId>
    (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: DoorWithoutIdAndStatus): DoorId {
    return controller.doorsService.create(requestBody).let(DoorId::of)
  }
}
