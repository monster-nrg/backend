package com.monsterenergy.backend.controllers.doors

import com.monsterenergy.backend.controllers.base.Controller
import com.monsterenergy.backend.controllers.doors.endpoints.*
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.services.DoorsService
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.routing.*
import io.ktor.util.*

class DoorsController(
  internal val doorsService: DoorsService
) : Controller {
  private companion object {
    const val id = "Id"

    fun Context.getDoorId() =
      call.parameters.getOrFail<Long>(id)
  }

  override fun mount(parentRoute: Route) {
    parentRoute.apply {
      authenticate {
        route("/doors") {
          get(GetAll(this@DoorsController))
          post(Create(this@DoorsController))
          get("/{${id}}", GetById(this@DoorsController) { getDoorId() })
          put("/{${id}}", Update(this@DoorsController) { getDoorId() })
          delete("/{${id}}", Delete(this@DoorsController) { getDoorId() })
        }
      }
    }
  }
}
