package com.monsterenergy.backend.controllers.doors.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.doors.DoorsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.door.Door

class GetById(
  controller: DoorsController,
  private val idSupplier: Context.() -> Long
) : Endpoint<DoorsController, Unit, Door>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): Door {
    return controller.doorsService.getById(idSupplier()).let(Door::of)
  }
}
