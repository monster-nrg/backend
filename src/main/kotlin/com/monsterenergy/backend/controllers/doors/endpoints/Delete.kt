package com.monsterenergy.backend.controllers.doors.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.doors.DoorsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass

class Delete(
  controller: DoorsController,
  private val idSupplier: Context.() -> Long
) : Endpoint<DoorsController, Unit, Unit>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit) {
    controller.doorsService.deleteById(idSupplier())
  }
}

