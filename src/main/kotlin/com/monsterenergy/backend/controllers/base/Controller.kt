package com.monsterenergy.backend.controllers.base

import io.ktor.routing.*

interface Controller {
  fun mount(parentRoute: Route)
}
