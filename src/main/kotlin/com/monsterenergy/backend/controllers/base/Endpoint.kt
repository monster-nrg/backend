package com.monsterenergy.backend.controllers.base

import com.monsterenergy.backend.controllers.util.Context
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.util.pipeline.*
import kotlin.reflect.KClass


abstract class Endpoint<TController : Controller, in TRequest : Any, TResponse : Any>(
  protected val controller: TController,
  private val requestType: KClass<TRequest>,
  private val responseType: KClass<TResponse>
) : PipelineInterceptor<Unit, ApplicationCall> {
  abstract suspend fun Context.handle(requestBody: TRequest): TResponse

  @Suppress("UNCHECKED_CAST")
  final override suspend fun invoke(pipeline: PipelineContext<Unit, ApplicationCall>, unit: Unit) {
    val body: TRequest = when (requestType) {
      Unit::class -> Unit as TRequest
      else -> pipeline.context.receive(requestType)
    }
    when (val response = pipeline.handle(body)) {
      Unit -> pipeline.context.respond(HttpStatusCode.NoContent)
      else -> pipeline.context.respond(response as Any)
    }
  }
}
