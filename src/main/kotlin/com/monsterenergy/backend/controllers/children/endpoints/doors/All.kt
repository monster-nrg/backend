package com.monsterenergy.backend.controllers.children.endpoints.doors

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.children.ChildrenController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.door.Door

class All(
  controller: ChildrenController,
  private val idSupplier: Context.() -> Long
) : Endpoint<ChildrenController, Unit, List<Door>>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): List<Door> {
    return controller.childrenService.getDoorsByChildId(idSupplier()).map(Door::of)
  }
}
