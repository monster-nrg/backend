package com.monsterenergy.backend.controllers.children

import com.monsterenergy.backend.controllers.base.Controller
import com.monsterenergy.backend.controllers.children.endpoints.*
import com.monsterenergy.backend.controllers.children.endpoints.doors.Add
import com.monsterenergy.backend.controllers.children.endpoints.doors.All
import com.monsterenergy.backend.controllers.children.endpoints.doors.Remove
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.services.ChildrenService
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.routing.*
import io.ktor.util.*

class ChildrenController(
  internal val childrenService: ChildrenService
) : Controller {
  private companion object {
    const val id = "Id"

    fun Context.getChildId() =
      call.parameters.getOrFail<Long>(id)
  }

  override fun mount(parentRoute: Route) {
    parentRoute.apply {
      authenticate {
        route("/children") {
          get(GetAll(this@ChildrenController))
          post(Create(this@ChildrenController))
          get("/{${id}}", GetById(this@ChildrenController) { getChildId() })
          put("/{${id}}", Update(this@ChildrenController) { getChildId() })
          delete("/{${id}}", Delete(this@ChildrenController) { getChildId() })
          route("/{$id}/doors") {
            get(All(this@ChildrenController) { getChildId() })
            put(Add(this@ChildrenController) { getChildId() })
            delete(Remove(this@ChildrenController) { getChildId() })
          }
        }
      }
    }
  }
}
