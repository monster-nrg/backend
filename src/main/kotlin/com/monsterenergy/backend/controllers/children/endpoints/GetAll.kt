package com.monsterenergy.backend.controllers.children.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.children.ChildrenController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.child.Child

class GetAll(controller: ChildrenController) :
  Endpoint<ChildrenController, Unit, List<Child>>
    (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): List<Child> {
    return controller.childrenService.getAll().map(Child::of)
  }
}
