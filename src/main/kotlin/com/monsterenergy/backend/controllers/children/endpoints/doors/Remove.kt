package com.monsterenergy.backend.controllers.children.endpoints.doors

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.children.ChildrenController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.door.Door
import com.monsterenergy.backend.dto.door.DoorId

class Remove(
  controller: ChildrenController,
  private val idSupplier: Context.() -> Long
) : Endpoint<ChildrenController, DoorId, List<Door>>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: DoorId): List<Door> {
    return controller.childrenService.removeDoorFromChild(idSupplier(), requestBody.id).map(Door::of)
  }
}
