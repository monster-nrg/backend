package com.monsterenergy.backend.controllers.children.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.children.ChildrenController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.child.Child

class GetById(
  controller: ChildrenController,
  private val idSupplier: Context.() -> Long
) : Endpoint<ChildrenController, Unit, Child>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): Child {
    return controller.childrenService.getById(idSupplier()).let(Child::of)
  }
}
