package com.monsterenergy.backend.controllers.children.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.children.ChildrenController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.child.ChildId
import com.monsterenergy.backend.dto.child.ChildWithoutId

class Create(controller: ChildrenController) :
  Endpoint<ChildrenController, ChildWithoutId, ChildId>
    (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: ChildWithoutId): ChildId {
    return controller.childrenService.create(requestBody).let(ChildId::of)
  }
}
