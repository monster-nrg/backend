package com.monsterenergy.backend.controllers.children.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.children.ChildrenController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass

class Delete(
  controller: ChildrenController,
  private val idSupplier: Context.() -> Long
) : Endpoint<ChildrenController, Unit, Unit>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit) {
    controller.childrenService.deleteById(idSupplier())
  }
}

