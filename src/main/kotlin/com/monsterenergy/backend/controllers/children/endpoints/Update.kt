package com.monsterenergy.backend.controllers.children.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.children.ChildrenController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.child.Child
import com.monsterenergy.backend.dto.child.ChildWithoutIdOptional
import com.monsterenergy.backend.util.toExposedBlob
import io.ktor.features.*

class Update(
  controller: ChildrenController,
  private val idSupplier: Context.() -> Long
) : Endpoint<ChildrenController, ChildWithoutIdOptional, Child>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: ChildWithoutIdOptional): Child {
    try {
      return controller.childrenService.update(idSupplier()) {
        requestBody.firstname?.let { this.firstname = it.orElse(null) }
        requestBody.surname?.let { this.surname = it.orElse(null) }
        requestBody.lastname?.let { this.lastname = it.orElse(null) }
        requestBody.birthDate?.let { this.birthDate = it.orElse(null) }
        requestBody.avatar?.let { this.avatar = it.map { blob -> blob.toExposedBlob() }.orElse(null) }
      }.let(Child::of)
    } catch (npe: NullPointerException) {
      throw BadRequestException("NPE", npe)
    }
  }
}
