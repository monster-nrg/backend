package com.monsterenergy.backend.controllers.departments.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.departments.DepartmentsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass

class Delete(
  controller: DepartmentsController,
  private val idSupplier: Context.() -> Long
) : Endpoint<DepartmentsController, Unit, Unit>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit) {
    controller.departmentsService.deleteById(idSupplier())
  }
}

