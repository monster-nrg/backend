package com.monsterenergy.backend.controllers.departments.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.departments.DepartmentsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.department.DepartmentId
import com.monsterenergy.backend.dto.department.DepartmentWithoutId

class Create(controller: DepartmentsController) :
  Endpoint<DepartmentsController, DepartmentWithoutId, DepartmentId>
    (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: DepartmentWithoutId): DepartmentId {
    return controller.departmentsService.create(requestBody).let(DepartmentId::of)
  }
}
