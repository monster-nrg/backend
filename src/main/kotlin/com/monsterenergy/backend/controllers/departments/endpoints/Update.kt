package com.monsterenergy.backend.controllers.departments.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.departments.DepartmentsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.department.Department
import com.monsterenergy.backend.dto.department.DepartmentWithoutIdOptional
import io.ktor.features.*

class Update(
  controller: DepartmentsController,
  private val idSupplier: Context.() -> Long
) :
  Endpoint<DepartmentsController, DepartmentWithoutIdOptional, Department>
    (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: DepartmentWithoutIdOptional): Department {
    try {
      return controller.departmentsService.update(idSupplier()) {
        requestBody.name?.let { this.name = it.orElse(null) }
      }.let(Department::of)
    } catch (npe: NullPointerException) {
      throw BadRequestException("NPE", npe)
    }
  }
}
