package com.monsterenergy.backend.controllers.departments.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.departments.DepartmentsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.department.Department

class GetById(
  controller: DepartmentsController,
  private val idSupplier: Context.() -> Long
) : Endpoint<DepartmentsController, Unit, Department>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): Department {
    return controller.departmentsService.getById(idSupplier()).let(Department::of)
  }
}
