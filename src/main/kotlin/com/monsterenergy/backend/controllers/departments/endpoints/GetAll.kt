package com.monsterenergy.backend.controllers.departments.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.departments.DepartmentsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.department.Department

class GetAll(controller: DepartmentsController) :
  Endpoint<DepartmentsController, Unit, List<Department>>
    (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): List<Department> {
    return controller.departmentsService.getAll().map(Department::of)
  }
}
