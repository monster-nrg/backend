package com.monsterenergy.backend.controllers.departments

import com.monsterenergy.backend.controllers.base.Controller
import com.monsterenergy.backend.controllers.departments.endpoints.*
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.services.DepartmentsService
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.routing.*
import io.ktor.util.*

class DepartmentsController(
  internal val departmentsService: DepartmentsService
) : Controller {
  private companion object {
    const val id = "Id"

    fun Context.getDepartmentId() =
      call.parameters.getOrFail<Long>(id)
  }

  override fun mount(parentRoute: Route) {
    parentRoute.apply {
      authenticate {
        route("/departments") {
          get(GetAll(this@DepartmentsController))
          post(Create(this@DepartmentsController))
          get("/{${id}}", GetById(this@DepartmentsController) { getDepartmentId() })
          put("/{${id}}", Update(this@DepartmentsController) { getDepartmentId() })
          delete("/{${id}}", Delete(this@DepartmentsController) { getDepartmentId() })
        }
      }
    }
  }
}
