package com.monsterenergy.backend.controllers.accounts.endpoints

import com.monsterenergy.backend.controllers.accounts.AccountsController
import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass

class Delete(
  controller: AccountsController,
  private val idSupplier: Context.() -> Long
) : Endpoint<AccountsController, Unit, Unit>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit) {
    controller.accountsService.deleteById(idSupplier())
  }
}

