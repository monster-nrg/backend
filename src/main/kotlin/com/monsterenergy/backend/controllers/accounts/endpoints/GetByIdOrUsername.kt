package com.monsterenergy.backend.controllers.accounts.endpoints

import com.monsterenergy.backend.controllers.accounts.AccountsController
import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.account.IdUsername

class GetByIdOrUsername(
  controller: AccountsController,
  private val idOrUsernameSupplier: Context.() -> String
) : Endpoint<AccountsController, Unit, IdUsername>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): IdUsername {
    return with(controller.accountsService) {
      when (val idOrUsername = idOrUsernameSupplier().let { it.toLongOrNull() ?: it }) {
        is Long -> getById(idOrUsername)
        is String -> getByUsername(idOrUsername)
        else -> throw IllegalStateException("Newer thrown")
      }
    }.let { IdUsername(it.id.value, it.username) }
  }
}
