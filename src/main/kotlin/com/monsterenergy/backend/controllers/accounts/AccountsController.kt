package com.monsterenergy.backend.controllers.accounts

import com.monsterenergy.backend.controllers.accounts.endpoints.*
import com.monsterenergy.backend.controllers.base.Controller
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.ControllerHelpers.getJwtPrincipal
import com.monsterenergy.backend.services.AccountsService
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.routing.*
import io.ktor.util.*

class AccountsController(
  internal val accountsService: AccountsService
) : Controller {
  private companion object {
    const val id = "IdOrUn"

    fun Context.getAccountIdOrUn() =
      call.parameters.getOrFail(id)

    fun Context.getAccountId() =
      call.parameters.getOrFail<Long>(id)
  }

  override fun mount(parentRoute: Route) {
    parentRoute.apply {
      route("/accounts") {
        post("/login", Login(this@AccountsController))
        authenticate {
          post("/logout", Logout(this@AccountsController) { getJwtPrincipal() })
          get(Current(this@AccountsController) { getJwtPrincipal() })
          route("/{${id}}") {
            get(GetByIdOrUsername(this@AccountsController) { getAccountIdOrUn() })
            put(Update(this@AccountsController) {getAccountId()})
            delete(Delete(this@AccountsController) {getAccountId()})
          }
        }
      }
    }
  }
}
