package com.monsterenergy.backend.controllers.accounts.endpoints

import com.monsterenergy.backend.controllers.accounts.AccountsController
import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.account.IdUsername
import io.ktor.auth.jwt.*
import java.util.*

class Current(
  controller: AccountsController,
  private val principalSupplier: Context.() -> JWTPrincipal
) : Endpoint<AccountsController, Unit, IdUsername>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): IdUsername {
    return controller.accountsService.getByTokenId(UUID.fromString(principalSupplier().jwtId))
      .let { IdUsername(it.id.value, it.username) }
  }
}
