package com.monsterenergy.backend.controllers.accounts.endpoints

import com.monsterenergy.backend.controllers.accounts.AccountsController
import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.account.IdUsername
import com.monsterenergy.backend.dto.account.UsernamePasswordOptional
import io.ktor.features.*
import org.mindrot.jbcrypt.BCrypt

class Update(
  controller: AccountsController,
  private val idSupplier: Context.() -> Long
) : Endpoint<AccountsController, UsernamePasswordOptional, IdUsername>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: UsernamePasswordOptional): IdUsername {
    try {
      return controller.accountsService.update(idSupplier()) {
        requestBody.username?.let { this.username = it.orElse(null) }
        requestBody.password?.let {
          this.password = it.map { pass -> BCrypt.hashpw(pass, BCrypt.gensalt()) }.orElse(null)
        }
      }.let(IdUsername::of)
    } catch (npe: NullPointerException) {
      throw BadRequestException("NPE", npe)
    }
  }
}
