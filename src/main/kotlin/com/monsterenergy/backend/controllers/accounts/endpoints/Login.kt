package com.monsterenergy.backend.controllers.accounts.endpoints

import com.monsterenergy.backend.controllers.accounts.AccountsController
import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.account.IdUsername
import com.monsterenergy.backend.dto.account.LoginSuccess
import com.monsterenergy.backend.dto.account.UsernamePassword

class Login(controller: AccountsController) :
  Endpoint<AccountsController, UsernamePassword, LoginSuccess>
    (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: UsernamePassword): LoginSuccess {
    return controller.accountsService.login(requestBody)
      .let { LoginSuccess(IdUsername(it.first.id.value, it.first.username), it.second) }
  }
}
