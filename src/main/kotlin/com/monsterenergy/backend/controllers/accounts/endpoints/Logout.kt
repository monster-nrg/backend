package com.monsterenergy.backend.controllers.accounts.endpoints

import com.monsterenergy.backend.controllers.accounts.AccountsController
import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.services.AuthenticationException
import io.ktor.auth.jwt.*
import java.util.*

class Logout(
  controller: AccountsController,
  private val principalGetter: Context.() -> JWTPrincipal
) : Endpoint<AccountsController, Unit, Unit>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit) {
    (principalGetter().jwtId ?: throw AuthenticationException())
      .let(UUID::fromString)
      .let { controller.accountsService.logout(it) }
  }
}
