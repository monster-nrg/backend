package com.monsterenergy.backend.controllers.jobs.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.jobs.JobsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.job.Job

class GetAll(controller: JobsController) :
  Endpoint<JobsController, Unit, List<Job>>
    (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): List<Job> {
    return controller.jobsService.getAll().map(Job::of)
  }
}
