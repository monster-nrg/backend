package com.monsterenergy.backend.controllers.jobs.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.jobs.JobsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.job.JobId
import com.monsterenergy.backend.dto.job.JobWithoutId

class Create(controller: JobsController) :
  Endpoint<JobsController, JobWithoutId, JobId>
    (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: JobWithoutId): JobId {
    return controller.jobsService.create(requestBody).let(JobId::of)
  }
}
