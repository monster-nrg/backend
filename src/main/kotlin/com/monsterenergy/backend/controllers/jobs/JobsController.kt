package com.monsterenergy.backend.controllers.jobs

import com.monsterenergy.backend.controllers.base.Controller
import com.monsterenergy.backend.controllers.jobs.endpoints.*
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.services.DepartmentsService
import com.monsterenergy.backend.services.JobsService
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.routing.*
import io.ktor.util.*

class JobsController(
  internal val jobsService: JobsService,
  internal val departmentsService: DepartmentsService
) : Controller {
  private companion object {
    const val id = "Id"

    fun Context.getJobId() =
      call.parameters.getOrFail<Long>(id)
  }

  override fun mount(parentRoute: Route) {
    parentRoute.apply {
      authenticate {
        route("/jobs") {
          get(GetAll(this@JobsController))
          post(Create(this@JobsController))
          get("/{${id}}", GetById(this@JobsController) { getJobId() })
          put("/{${id}}", Update(this@JobsController) { getJobId() })
          delete("/{${id}}", Delete(this@JobsController) { getJobId() })
        }
      }
    }
  }
}
