package com.monsterenergy.backend.controllers.jobs.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.jobs.JobsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.job.Job

class GetById(
  controller: JobsController,
  private val idSupplier: Context.() -> Long
) : Endpoint<JobsController, Unit, Job>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): Job {
    return controller.jobsService.getById(idSupplier()).let(Job::of)
  }
}
