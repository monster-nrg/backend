package com.monsterenergy.backend.controllers.jobs.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.jobs.JobsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.job.Job
import com.monsterenergy.backend.dto.job.JobWithoutIdOptional
import io.ktor.features.*

class Update(
  controller: JobsController,
  private val idSupplier: Context.() -> Long
) :
  Endpoint<JobsController, JobWithoutIdOptional, Job>
    (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: JobWithoutIdOptional): Job {
    try {
      return controller.jobsService.update(idSupplier()) {
        requestBody.name?.let { this.name = it.orElse(null) }
        requestBody.departmentId?.let {
          this.department = controller.departmentsService.getById(it.orElse(null))
        }
      }.let(Job::of)
    } catch (npe: NullPointerException) {
      throw BadRequestException("NPE", npe)
    }
  }
}
