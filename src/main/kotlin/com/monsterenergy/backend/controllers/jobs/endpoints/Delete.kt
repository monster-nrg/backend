package com.monsterenergy.backend.controllers.jobs.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.jobs.JobsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass

class Delete(
  controller: JobsController,
  private val idSupplier: Context.() -> Long
) : Endpoint<JobsController, Unit, Unit>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit) {
    controller.jobsService.deleteById(idSupplier())
  }
}

