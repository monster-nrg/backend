package com.monsterenergy.backend.controllers.permissions.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.permissions.PermissionsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.permission.Permission

class GetAll(controller: PermissionsController) :
  Endpoint<PermissionsController, Unit, List<Permission>>
    (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): List<Permission> {
    return controller.permissionsService.getAll().map(Permission::of)
  }
}
