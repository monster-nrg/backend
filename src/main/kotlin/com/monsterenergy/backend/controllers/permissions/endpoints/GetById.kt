package com.monsterenergy.backend.controllers.permissions.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.permissions.PermissionsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.permission.Permission

class GetById(
  controller: PermissionsController,
  private val idSupplier: Context.() -> Long
) : Endpoint<PermissionsController, Unit, Permission>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): Permission {
    return controller.permissionsService.getById(idSupplier()).let(Permission::of)
  }
}
