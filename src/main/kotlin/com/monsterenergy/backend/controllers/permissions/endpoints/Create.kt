package com.monsterenergy.backend.controllers.permissions.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.permissions.PermissionsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.permission.PermissionId
import com.monsterenergy.backend.dto.permission.PermissionWithoutId

class Create(controller: PermissionsController) :
  Endpoint<PermissionsController, PermissionWithoutId, PermissionId>
    (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: PermissionWithoutId): PermissionId {
    return controller.permissionsService.create(requestBody).let(PermissionId::of)
  }
}
