package com.monsterenergy.backend.controllers.permissions.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.permissions.PermissionsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass

class Delete(
  controller: PermissionsController,
  private val idSupplier: Context.() -> Long
) : Endpoint<PermissionsController, Unit, Unit>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit) {
    controller.permissionsService.deleteById(idSupplier())
  }
}

