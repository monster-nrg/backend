package com.monsterenergy.backend.controllers.permissions.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.permissions.PermissionsController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.permission.Permission
import com.monsterenergy.backend.dto.permission.PermissionWithoutIdOptional
import io.ktor.features.*

class Update(
  controller: PermissionsController,
  private val idSupplier: Context.() -> Long
) : Endpoint<PermissionsController, PermissionWithoutIdOptional, Permission>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: PermissionWithoutIdOptional): Permission {
    try {
      return controller.permissionsService.update(idSupplier()) {
        requestBody.codename?.let { this.codename = it.orElse(null) }
        requestBody.name?.let { this.name = it.orElse(null) }
      }.let(Permission::of)
    } catch (e: NullPointerException) {
      throw BadRequestException("NPE", e)
    }
  }
}
