package com.monsterenergy.backend.controllers.permissions

import com.monsterenergy.backend.controllers.base.Controller
import com.monsterenergy.backend.controllers.permissions.endpoints.*
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.services.PermissionsService
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.routing.*
import io.ktor.util.*

class PermissionsController(
  internal val permissionsService: PermissionsService
) : Controller {
  private companion object {
    const val id = "permissionId"

    fun Context.getPermissionId() =
      call.parameters.getOrFail<Long>(id)
  }

  override fun mount(parentRoute: Route) {
    parentRoute.apply {
      authenticate {
        route("/permissions") {
          get(GetAll(this@PermissionsController))
          post(Create(this@PermissionsController))
          get("/{${id}}", GetById(this@PermissionsController) { getPermissionId() })
          put("/{${id}}", Update(this@PermissionsController) { getPermissionId() })
          delete("/{${id}}", Delete(this@PermissionsController) { getPermissionId() })
        }
      }
    }
  }
}

