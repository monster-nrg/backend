package com.monsterenergy.backend.controllers.users.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.users.UsersController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.user.User
import com.monsterenergy.backend.dto.user.UserWithoutId

class Create(controller: UsersController) :
  Endpoint<UsersController, UserWithoutId, User>
    (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: UserWithoutId): User {
    return controller.usersService.create(requestBody).let(User::of)
  }
}
