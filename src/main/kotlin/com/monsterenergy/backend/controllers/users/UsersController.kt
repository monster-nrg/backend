package com.monsterenergy.backend.controllers.users

import com.monsterenergy.backend.controllers.base.Controller
import com.monsterenergy.backend.controllers.users.endpoints.*
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.services.UsersService
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.routing.*
import io.ktor.util.*


class UsersController(internal val usersService: UsersService) : Controller {
  companion object {
    const val id = "id"

    fun Context.getUserId(): Long =
      call.parameters.getOrFail<Long>(id)
  }

  override fun mount(parentRoute: Route) {
    parentRoute.apply {
      authenticate {
        route("/users") {
          get(GetAll(this@UsersController))
          post(Create(this@UsersController))
          route("/{$id}") {
            get(GetById(this@UsersController) { getUserId() })
            get("/jobs", GetJobsByUserId(this@UsersController) { getUserId() })
            put(Update(this@UsersController) { getUserId() })
            delete(Delete(this@UsersController) { getUserId() })
          }
        }
      }
    }
  }
}
