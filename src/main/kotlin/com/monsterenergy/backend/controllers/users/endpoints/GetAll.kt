package com.monsterenergy.backend.controllers.users.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.users.UsersController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.user.User

class GetAll(controller: UsersController) :
  Endpoint<UsersController, Unit, List<User>>
    (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): List<User> {
    return controller.usersService.getAll().map(User::of)
  }
}
