package com.monsterenergy.backend.controllers.users.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.users.UsersController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.user.User
import com.monsterenergy.backend.dto.user.UserWithoutIdOptional
import com.monsterenergy.backend.util.toExposedBlob
import io.ktor.features.*

class Update(
  controller: UsersController,
  private val idSupplier: Context.() -> Long
) : Endpoint<UsersController, UserWithoutIdOptional, User>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: UserWithoutIdOptional): User {
    try {
      return controller.usersService.update(idSupplier()) {
        requestBody.firstname?.let { this.firstname = it.orElse(null) }
        requestBody.surname?.let { this.surname = it.orElse(null) }
        requestBody.lastname?.let { this.lastname = it.orElse(null) }
        requestBody.birthDate?.let { this.birthDate = it.orElse(null) }
        requestBody.avatar?.let { this.avatar = it.map { blob -> blob.toExposedBlob() }.orElse(null) }
      }.let(User::of)
    } catch (npe: NullPointerException) {
      throw BadRequestException("NPE", npe)
    }
  }
}
