package com.monsterenergy.backend.controllers.users.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.users.UsersController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass

class Delete(
  controller: UsersController,
  private val idSupplier: Context.() -> Long
) : Endpoint<UsersController, Unit, Unit>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit) {
    controller.usersService.deleteById(idSupplier())
  }
}

