package com.monsterenergy.backend.controllers.users.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.users.UsersController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.job.Job

class GetJobsByUserId(
  controller: UsersController,
  private val idSupplier: Context.() -> Long
) : Endpoint<UsersController, Unit, List<Job>>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): List<Job> {
    return controller.usersService.getUserJobs(idSupplier()).map(Job::of)
  }
}
