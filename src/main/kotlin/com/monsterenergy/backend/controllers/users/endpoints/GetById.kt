package com.monsterenergy.backend.controllers.users.endpoints

import com.monsterenergy.backend.controllers.base.Endpoint
import com.monsterenergy.backend.controllers.users.UsersController
import com.monsterenergy.backend.controllers.util.Context
import com.monsterenergy.backend.controllers.util.getKClass
import com.monsterenergy.backend.dto.user.User

class GetById(
  controller: UsersController,
  private val idSupplier: Context.() -> Long
) : Endpoint<UsersController, Unit, User>
  (controller, getKClass(), getKClass()) {
  override suspend fun Context.handle(requestBody: Unit): User {
    return controller.usersService.getById(idSupplier()).let(User::of)
  }
}
